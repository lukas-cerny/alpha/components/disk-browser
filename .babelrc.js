module.exports = {
    sourceType: 'module',
    presets: [
        [
            '@babel/env',
            {
                loose: true,
                modules: false,
                exclude: ['transform-typeof-symbol']
            }
        ]
    ],
    plugins: [
        '@babel/plugin-transform-object-assign',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-property-mutators',
        '@babel/plugin-transform-reserved-words',
        'babel-plugin-transform-member-expression-literals',
        'babel-plugin-transform-property-literals'
    ],
    env: {
        test: {
            plugins: ['istanbul']
        }
    }
};