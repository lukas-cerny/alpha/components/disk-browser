<?php

namespace Alpha\Component\DiskBrowser\Components\DiskBrowser;

use Alpha\Component\DiskBrowser\Components\IFileGridFactory;
use Alpha\Component\DiskBrowser\Components\IUploadControlFactory;
use Alpha\Component\DiskBrowser\Entity;
use Alpha\Component\DiskBrowser\Facade;
use Alpha\Component\DiskBrowser\RESTModule\Presenters\ApiPresenter;
use Alpha\Component\SecuredComponents\Authorizator;
use Alpha\Component\SecuredComponents\SecuredComponentTrait;
use Alpha\Utils\Arrays;
use Alpha\Utils\Exceptions\InvalidStateException;
use Alpha\Utils\UI\DatabaseControl;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\UI\Control;
use Nette\Http\Response;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends DatabaseControl
{
    use SecuredComponentTrait;
    const NAME = 'DiskBrowser';
    /** @var string */
    private $cipher = '';
    /** @persistent */
    public $uid = null;
    /** @persistent */
    public $selectedId;
    private $configuration = [
        'savePath' => '',
        'addRoot' => true,
        'selectFirst' => false,
        'expandRoot' => false,
        'resize' => []
    ];
    public $contextMenu = [

    ];

    /** @var Facade\Directory */
    private $facade;
    /** @var IUploadControlFactory */
    private $factoryUpload;
    private $factoryFileGrid;

    public function __construct(Control $parent, $name, EntityManagerInterface $em, array $configuration, Entity\Directory $directory,
                                IUploadControlFactory $factoryUpload, IFileGridFactory $fileGridFactory)
    {
        parent::__construct($parent, $name, $em, $directory);
        $this->uid = $this->getSnippetId("general");
        $this->em = $em;
        $this->factoryFileGrid = $fileGridFactory;
        $this->facade = new Facade\Directory($em);
        $this->configuration = Arrays::merge($this->configuration, $configuration);
        $this->factoryUpload = $factoryUpload;
    }

    public function handleDirectoryCreate()
    {
        $request = $this->getPresenter()->getRequest();
        $response = [
            'status' => 'success'
        ];

        try {
            if (!$this->isAllowedOperation(Authorizator::CREATE, self::NAME)) {
                $this->getPresenter()->getHttpResponse()->setCode(Response::S403_FORBIDDEN);
                throw new InvalidStateException('Nemáte dostatečná oprávnění k vytváření složek');
            }

            $entity = ApiPresenter::createDirectory($this->getPresenter()->getHttpResponse(), $this->facade, $request->getPost('text'), $request->getPost('parent_id'));

            $response['id'] = $entity->id;
        } catch (InvalidStateException $e) {
            if ($this->getPresenter()->getHttpResponse()->getCode() === Response::S200_OK)
                $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);

            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        } finally {
            $this->getPresenter()->sendJson($response);
        }
    }

    public function handleDirectoryRead($id)
    {
        $data = [];
        if ($this->isAllowedOperation(Authorizator::READ, self::NAME)) {
            if ($this->facade->getById($id, false)) {
                $data = $this->facade->getItemTree($id);
                if (count($data) && $id == $this->entity->getId()) {
                    $data = $data['nodes'];

                    if ($this->configuration['selectFirst']) {
                        if (is_array($data) && count($data) && !array_key_exists('id', $data)) {
                            $data[0]['states'] = [];
                            $data[0]['states']['selected'] = true;
                        }
                    }
                }
            } else $this->getPresenter()->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
        } else $this->getPresenter()->getHttpResponse()->setCode(Response::S403_FORBIDDEN);

        $this->getPresenter()->sendJson($data);
    }

    public function handleDirectoryUpdate()
    {
        $request = $this->getPresenter()->getRequest();
        $response = [
            'status' => 'success'
        ];

        try {
            if (!$this->isAllowedOperation(Authorizator::UPDATE, self::NAME)) {
                $this->getPresenter()->getHttpResponse()->setCode(Response::S403_FORBIDDEN);
                throw new InvalidStateException('Nemáte dostatečná oprávnění k úpravě složeky');
            }

            if (!$this->facade->isChild($this->entity->getId(), $request->getPost('id'))) {
                $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);
                throw new InvalidStateException('Tuto složku nelze upravit');
            }

            if ($this->entity->getId() !== (int)$request->getPost('id')) {
                $parent = $this->facade->getParent($request->getPost('id'));
                if ($this->facade->getChildDirectory($parent->getId(), $request->getPost('text'))) {
                    $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);
                    throw new InvalidStateException('Složka se stejným názvem již existuje');
                }
            }

            $entity = $this->facade->getById($request->getPost('id'));
            $entity->name = $request->getPost('text');
            $this->facade->updateEntity($entity, true, true);
        } catch (InvalidStateException $e) {
            if ($this->getPresenter()->getHttpResponse()->getCode() === Response::S200_OK)
                $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);

            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        } finally {
            $this->getPresenter()->sendJson($response);
        }
    }

    public function handleDirectoryDelete()
    {
        $request = $this->getPresenter()->getRequest();
        $response = [
            'status' => 'success'
        ];

        try {
            if (!$this->isAllowedOperation(Authorizator::DELETE, self::NAME) || $this->entity->getId() === $request->getPost('id')) {
                $this->getPresenter()->getHttpResponse()->setCode(Response::S403_FORBIDDEN);
                throw new InvalidStateException('Nemáte dostatečná oprávnění k smazání složky');
            }

            if (!$this->facade->isChild($this->entity->getId(), $request->getPost('id'))) {
                $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);
                throw new InvalidStateException('Tuto složku nelze smazat');
            }

            $this->facade->deleteById($request->getPost('id'), true, true);
        } catch (InvalidStateException $e) {
            if ($this->getPresenter()->getHttpResponse()->getCode() === Response::S200_OK)
                $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);

            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        } finally {
            $this->getPresenter()->sendJson($response);
        }
    }

    public function handleDirectoryChange()
    {
        $this->selectedId = $this->getPresenter()->getRequest()->getPost('id');
        $this->redrawControl('files');
        $this['upload']->redrawControl();
    }

    /**
     * @param string $cipher
     */
    public function setCipher($cipher)
    {
        $this->cipher = $cipher;
    }

    public function render(array $params = null)
    {
        $template = $this->template;
        $template->id = $this->uid;
        $template->root = $this->entity;
        $template->currentDirId = $this->selectedId === null ? $this->entity->getId() : $this->selectedId;
        $template->configuration = (object)[
            'create' => $this->isAllowedOperation(Authorizator::CREATE, self::NAME),
            'read' => $this->isAllowedOperation(Authorizator::READ, self::NAME),
            'update' => $this->isAllowedOperation(Authorizator::UPDATE, self::NAME),
            'delete' => $this->isAllowedOperation(Authorizator::DELETE, self::NAME),
            'upload' => $this->selectedId !== null,
            'displayFiles' => $this->selectedId !== null,
        ];

        $template->render(__DIR__ . '/default.latte');
    }

    protected function createComponentFiles($name)
    {
        if ($this->selectedId === null) {
            $this->selectedId = $this->entity->getId();
            $directory = $this->entity;
        } else {
            $directory = $this->facade->getById($this->selectedId);
            $this->selectedId = $directory->getId();
        }
        return $this->factoryFileGrid->create($this, $name, $directory);
    }

    protected function createComponentUpload($name)
    {
        if ($this->selectedId !== null) {
            $directory = $this->facade->getById($this->selectedId);
            $this->selectedId = $directory->getId();
        } else
            $directory = $this->getEntity();
        $component = $this->factoryUpload->create($this, $name, [
            'create' => $this->isAllowedOperation(Authorizator::CREATE, self::NAME),
            'popup' => true,
            'only_images' => array_key_exists('only_images', $this->configuration) && $this->configuration['only_images']
        ], $directory);
        $that = $this;
        $component->callback = function ($entity) use ($that) {
            $that->redrawControl('files');
        };

        return $component;
    }
}
