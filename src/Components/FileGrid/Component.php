<?php

namespace Alpha\Component\DiskBrowser\Components\FileGrid;

use Alpha\Component\DiskBrowser\Entity;
use Alpha\Component\DiskBrowser\Facade;
use Alpha\Component\DiskBrowser\RESTModule\Presenters\ApiPresenter;
use Alpha\Component\SecuredComponents\Authorizator;
use Alpha\Component\SecuredComponents\SecuredComponentTrait;
use Alpha\System\Components\General\Flash\Control as Flash;
use Alpha\Utils\Exceptions\InvalidStateException;
use Alpha\Utils\Strings;
use Alpha\Utils\UI\DatabaseControl;
use Alpha\Utils\UI\Form\BaseFormTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\UI\Control;
use Nette\Http\Response;
use Nette\Utils\Json;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends DatabaseControl
{
    use SecuredComponentTrait;
    use BaseFormTrait;

    const NAME = 'FileGrid';
    /** @var string */
    private $cipher = '';
    private $files = [];
    /** @var File */
    private $facade;
    /** @var Facade\Directory */
    private $facadeDirectory;

    public function __construct(Control $parent, $name, Entity\Directory $directory, EntityManagerInterface $em)
    {
        parent::__construct($parent, $name, $em, $directory);
        $this->em = $em;
        $this->facade = new Facade\File($em);
        $this->facadeDirectory = new Facade\Directory($em);
    }

    public function render(array $params = null)
    {
        if ($this->entity === null || $this->entity->files === null)
            $this->files = [];
        else
            $this->files = $this->entity->files->toArray();

        $template = $this->template;
        $template->uid = $this->getUID('files');
        $template->files = $this->files;
        $template->filesId = '#' . $this->getSnippetId('files');
        $template->token = Strings::encrypt(Json::encode([
            'name' => $this->getResourceName(self::NAME),
            'directory' => $this->entity->getId()
        ]), $this->cipher);
        $template->configuration = (object)[
            'create' => $this->isAllowedOperation(Authorizator::CREATE, self::NAME),
            'read' => $this->isAllowedOperation(Authorizator::READ, self::NAME),
            'update' => $this->isAllowedOperation(Authorizator::UPDATE, self::NAME),
            'delete' => $this->isAllowedOperation(Authorizator::DELETE, self::NAME)
        ];
        $template->render(__DIR__ . '/default.latte');
    }

    public function handleRenameFile($uid)
    {
        $request = $this->getPresenter()->getRequest();

        try {
            if (!$this->isAllowedOperation(Authorizator::UPDATE, self::NAME)) {
                $this->getPresenter()->getHttpResponse()->setCode(Response::S403_FORBIDDEN);
                throw new InvalidStateException('Nemáte dostatečná oprávnění k úpravě souboru');
            }

            ApiPresenter::updateFile($this->presenter->getHttpResponse(), (object) ['name' => $request->getPost('name')], $this->facade, $this->facadeDirectory, $uid, 'rename', $this->entity->getId());
        } catch (InvalidStateException $e) {
            if ($this->getPresenter()->getHttpResponse()->getCode() === Response::S200_OK)
                $this->getPresenter()->getHttpResponse()->setCode(Response::S400_BAD_REQUEST);
            $this->flashMessage($e->getMessage(), Flash::DANGER);
        }
        $this->redrawControl('files');
    }

    public function handleDeleteFile($uid)
    {
        try {
            if (!$this->isAllowedOperation(Authorizator::DELETE, self::NAME))
                throw new InvalidStateException('Nemáte dostatečná oprávnění pro smazání souboru');

            ApiPresenter::deleteFile($this->facade, $uid, $this->entity->getId());
            $this->redrawControl('files');
        } catch (InvalidStateException $e) {
            $this->flashMessage($e->getMessage(), Flash::DANGER);
        }
    }

    /**
     * @param string $cipher
     */
    public function setCipher($cipher)
    {
        $this->cipher = $cipher;
    }
}