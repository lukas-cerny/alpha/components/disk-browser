<?php

namespace Alpha\Component\DiskBrowser\Components;

use Alpha\Component\DiskBrowser\Entity;
use Nette\Application\UI\Control;

interface IDiskBrowserFactory
{

    /**
     * @param Control          $parent
     * @param string           $name
     * @param array            $configuration
     * @param Entity\Directory $directory
     * @return DiskBrowser\Component
     */
    function create(Control $parent, $name, array $configuration, Entity\Directory $directory);
}
