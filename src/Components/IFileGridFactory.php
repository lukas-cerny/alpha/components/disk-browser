<?php

namespace Alpha\Component\DiskBrowser\Components;

use Alpha\Component\DiskBrowser\Entity;
use Nette\Application\UI\Control;

interface IFileGridFactory
{
    /**
     * @param Control          $parent
     * @param string           $name
     * @param Entity\Directory $directory
     * @return FileGrid\Component
     */
    function create(Control $parent, $name, Entity\Directory $directory);
}