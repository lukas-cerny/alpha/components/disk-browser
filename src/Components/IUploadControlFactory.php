<?php

namespace Alpha\Component\DiskBrowser\Components;

use Alpha\Component\DiskBrowser\Entity;
use Nette\Application\UI\Control;

interface IUploadControlFactory
{

    /**
     * @param Control $parent
     * @param string $name
     * @param array $configuration
     * @param Entity\Directory $directory
     * @return UploadControl\Component
     */
    function create(Control $parent, $name, array $configuration, Entity\Directory $directory = null);
}
