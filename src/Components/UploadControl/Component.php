<?php

namespace Alpha\Component\DiskBrowser\Components\UploadControl;

use Alpha\Component\DiskBrowser\Entity;
use Alpha\Component\DiskBrowser\Facade\Directory;
use Alpha\Component\DiskBrowser\Facade\File;
use Alpha\Component\SecuredComponents\Authorizator;
use Alpha\Component\SecuredComponents\SecuredComponentTrait;
use Alpha\Utils\Arrays;
use Alpha\Utils\Exceptions\InvalidArgumentException;
use Alpha\Utils\Exceptions\InvalidStateException;
use Alpha\Utils\UI\DatabaseControl;
use Alpha\Utils\UI\Form\BaseFormTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\Request;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;
use Nette\Http\Response;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;
use Nette\Utils\Image;
use Nette\Utils\Json;
use Nette\Utils\Random;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends DatabaseControl
{
    use SecuredComponentTrait;
    use BaseFormTrait;
    const NAME = 'UploadFiles';
    /** @var File */
    private $facadeFile;
    /** @var Directory */
    private $facadeDirectory;
    /** @var Entity\Directory */
    private $rootDir = null;
    private $configuration = [
        'only_images' => false,
        'resize_on' => false,
        'popup' => false,
        'extensions' => [],
        'id' => null,
        'create' => true,
    ];

    /**
     * Component constructor.
     *
     * @param Control                $parent
     * @param                        $name
     * @param array                  $configuration
     *                                             [
     *                                             path => '' -- Required
     * @param EntityManagerInterface $em
     * @param Entity\Directory|null  $directory
     */
    public function __construct(Control $parent, $name, array $configuration, EntityManagerInterface $em, File $facadeFile,
                                Directory $facadeDirectory, Entity\Directory $directory = null)
    {
        parent::__construct($parent, $name, $em, $directory);
        $this->facadeFile = $facadeFile;
        $this->facadeDirectory = $facadeDirectory;
        $this->validateConfiguration($configuration);
        $this->rootDir = $directory;
    }

    public function render(array $params = null)
    {
        $template = $this->template;
        if ($params !== null && array_key_exists('id', $params))
            $template->id = $params['id'];
        else if ($this->configuration['id'] !== null)
            $template->id = $this->configuration['id'];
        else
            $template->id = $this->getUID('general');
        $defaults = [
            'icon' => false,
            'class' => 'btn-outline-primary',
            'title' => 'Nahrát soubor',
            'close' => 'Close'
        ];
        if ($params !== null) {
            if (array_key_exists('icon', $params) && $params['icon']) {
                $defaults = [
                    'icon' => true,
                    'class' => 'fa fa-upload',
                    'title' => 'Nahrát soubor',
                    'close' => 'Close'
                ];
            }
            $defaults = Arrays::merge($defaults, (array) $params);
        }
        $template->configuration = (object) $defaults;
        $template->directoryId = $this->rootDir->getId();
        $template->render(__DIR__ . '/default.latte');
    }

    public function setPath($path)
    {
        $this->configuration['path'] = $this->presenter->context->getParameters()['wwwDir'] . '/' . $path;
    }

    public function setResize($resize)
    {
        $this->configuration['resize'] = [];
        $this->validateConfiguration(['resize' => $resize]);
    }

    public function handleUpload()
    {
        $request = $this->getPresenter()->getRequest();
        if ($user = $this->getPresenter()->getUser()) {
            $subDir = $user->getId() . '/';
        } else {
            $subDir = null;
        }

        $files = $request->getFiles();
        /** @var FileUpload $file */
        foreach ($files as $file) {
            $path = $this->configuration['path'] . $subDir . $request->getPost('uid');
            $ext = $this->getExtensionOfFile($request->getPost('name'));
            $file->move($path . '/' . $request->getPost('part_index') . $ext);
            $entity = new Entity\File();
            try {
                $attached = (array)Json::decode($request->getPost('attached_data', '[]'));
                if (!array_key_exists('directory_id', $attached) ||
                    ($this->rootDir->getId() !== $attached['directory_id']
                        && !$this->facadeDirectory->isChild($this->rootDir->getId(), $attached['directory_id'])))// Check if file with same name exists
                    throw new InvalidStateException('Bad directory ID');
                if ($this->facadeDirectory->fileExists($request->getPost('name'), $attached['directory_id']))
                    throw new InvalidStateException('File with same name already exists.');

                if (count(Finder::findFiles('*' . $ext)->in($path . '/')) == $request->getPost('part_count')) {
                    $this->uploadComplete($request, $path, $ext, $entity, $subDir);
                    $this->callCallbacks([$entity]);
                }
            } catch (InvalidStateException $e) {
                FileSystem::delete($path . $ext);
                $response = $this->getPresenter()->getHttpResponse();
                $response->setCode(Response::S409_CONFLICT);
            }
        }
    }

    public function getExtensionOfFile($fileName)
    {
        $parts = explode('.', $fileName);
        if (count($parts) > 1)
            return '.' . array_pop($parts);
        return '';
    }

    /**
     * @param Request     $request
     * @param             $path
     * @param             $ext
     * @param Entity\File $entity
     * @param             $subDir
     * @throws \Nette\Utils\UnknownImageFileException
     * @throws InvalidStateException
     */
    public function uploadComplete(Request $request, $path, $ext, Entity\File &$entity, $subDir)
    {
        // concat parts
        for ($i = 0; $i < $request->getPost('part_count'); $i++) {
            $content = file_get_contents($path . '/' . $i . $ext);
            file_put_contents($path . $ext, $content, FILE_APPEND);
        }
        // delete parts of file
        FileSystem::delete($path . '/');

        // init entity
        $entity->setUName(Random::generate(10));
        $entity->setName($request->getPost('name'));
        $entity->setMimeType($request->getPost('type'));
        $entity->setStoredName($subDir . $request->getPost('uid') . $ext);
        $entity->setDirectory($this->rootDir);

        if ($this->configuration['extensions'] !== null && count($this->configuration['extensions']) && !in_array($entity->getMimeType(), $this->configuration['extensions'])) {
            throw new InvalidStateException('Not supported extension (' . $entity->getMimeType() . ')');
        }

        if ($this->configuration['only_images'] && false === $entity->isImage()) {
            throw new InvalidStateException('Not supported extension');
        }

        $this->facadeFile->createEntity($entity, true, true);
        if (in_array($entity->getMimeType(), ['image/gif', 'image/png', 'image/jpeg'], true)) {
            $image = Image::fromFile($path . $ext);
            usort($this->configuration['resize'], function ($a, $b) {
                if ($a['height'] === null && $b['height'] === null) {
                    if ($a['width'] === $b['width']) {
                        return true;
                    } else {
                        if ($a['width'] === null)
                            return false;
                        elseif ($b['width'] === null)
                            return true;
                        else return $a['width'] <= $b['width'];
                    }
                } elseif ($a['height'] === null)
                    return false;
                elseif ($b['height'] === null)
                    return true;
                else return $a['height'] <= $b['height'];
            });
            foreach ($this->configuration['resize'] as $resize) {
                $image->resize($resize['width'], $resize['height']);
                $image->save($this->configuration['path'] . $subDir
                    . ($resize['pre'] === null ? '' : $resize['pre'] . '_')
                    . $request->getPost('uid')
                    . ($resize['last'] === null ? '' : '_' . $resize['last'])
                    . $ext);
            }
        }
    }

    public function validateConfiguration(array $configuration)
    {
        $resize = [
            'last' => null,
            'pre' => null,
            'width' => null,
            'height' => null,
            'quality' => 100,
        ];

        foreach ($configuration as $key => $value) {
            if (array_key_exists($key, $this->configuration)) {
                if ($key === 'resize') {
                    foreach ($value as $setting) {
                        $this->configuration['resize'][] = Arrays::merge($resize, $setting);
                    }
                } else
                    $this->configuration[$key] = $value;
            } else throw new InvalidArgumentException("Key {$key} is unexpected i configuration file");
        }
    }
}