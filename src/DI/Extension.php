<?php

namespace Alpha\Component\DiskBrowser\DI;

use Alpha\Component\DiskBrowser\Components\IDiskBrowserFactory;
use Alpha\Component\DiskBrowser\Components\IFileGridFactory;
use Alpha\Component\DiskBrowser\Components\IUploadControlFactory;
use Alpha\Component\DiskBrowser\Fixtures\InitData;
use Alpha\Component\DiskBrowser\RESTModule\Presenters\ApiPresenter;
use Alpha\DI\CompilerExtension;
use Alpha\DI\IEntityProvider;
use Alpha\Utils\Arrays;
use Alpha\Utils\Database\IDataFixture;
use Nette;

/**
 * @package Alpha\Component\DiskBrowser
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Extension extends CompilerExtension implements IEntityProvider, IDataFixture
{
    private $defaults = [
        "cipher" => "_Test:Value_123_",
        "upload" => [
            "dir" => "upload/",
            "resize" => [
                ["last" => "x-large", "height" => 1000],
                ["last" => "large", "height" => 500],
                ["last" => "medium", "height" => 250],
                ["last" => "small", "height" => 100]
            ]
        ]
    ];

    public static function getMappingEntities()
    {
        return ["Alpha\\Component\\DiskBrowser\\Entity" => __DIR__ . '/../Entity'];
    }

    public static function getFixtures()
    {
        return [
            InitData::class
        ];
    }

    public function afterCompile(Nette\PhpGenerator\ClassType $class)
    {
        $this->getConfig($this->defaults);
    }


    public function loadConfiguration()
    {
        $this->parseConfigFile(__DIR__ . '/config.json');
        $this->setConfig(Arrays::merge($this->defaults, $this->config));

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix("diskBrowser"))
            ->setImplement(IDiskBrowserFactory::class)
            ->addSetup('setCipher', ['cipher' => $this->config["cipher"]]);
        $builder->addDefinition($this->prefix("fileGrid"))
            ->setImplement(IFileGridFactory::class)
            ->addSetup('setCipher', ['cipher' => $this->config["cipher"]]);
        $builder->addDefinition($this->prefix("upload"))
            ->setImplement(IUploadControlFactory::class)
            ->addSetup("setPath", ["path" => $this->config["upload"]["dir"]])
            ->addSetup("setResize", ['resize' => $this->config["upload"]["resize"]]);

        $builder->addDefinition($this->prefix("rest"))
            ->setType(ApiPresenter::class)
            ->addSetup("setPath", ["path" => $this->config["upload"]["dir"]])
            ->addSetup("setResize", ['resize' => $this->config["upload"]["resize"]])
            ->addSetup('setCipher', ['cipher' => $this->config["cipher"]]);
    }

    public function beforeCompile()
    {
        $this->addPresenterMapping(["ComponentDiskBrowser" => "Alpha\\Component\\DiskBrowser\\*Module\\Presenters\\*Presenter"]);
    }


}