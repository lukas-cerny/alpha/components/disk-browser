<?php

namespace Alpha\Component\DiskBrowser\Entity;

use Alpha\Utils\Database\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Table(name="component_disk_browser_directory")
 * @ORM\Entity
 */
class Directory extends BaseEntity
{
    use MagicAccessors;

    const NAME_MAX_LENGTH = 32;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false, unique=false)
     */
    protected $name;
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_root", type="boolean")
     */
    protected $root = false;
    /**
     * @var File[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="File", mappedBy="directory", indexBy="id")
     */
    protected $files;

    /**
     * Directory constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection;
    }

    /**
     * @param bool $root
     * @return $this
     */
    public function setRoot($root = true)
    {
        $this->root = $root;
        return $this;
    }

    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    public function removeFile(File $file)
    {
        $this->files->removeElement($file);
    }

}