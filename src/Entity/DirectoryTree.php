<?php

namespace Alpha\Component\DiskBrowser\Entity;

use Alpha\Utils\Database\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Table(name="component_disk_browser_directory_tree", indexes={@ORM\Index(name="ancestor_id", columns={"ancestor_id"}), @ORM\Index(name="descendant_id", columns={"descendant_id"})})
 * @ORM\Entity
 */
class DirectoryTree extends BaseEntity
{
    use MagicAccessors;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    protected $depth = 0;
    /**
     * @var Directory
     *
     * @ORM\ManyToOne(targetEntity="Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="ancestor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $ancestor = null;
    /**
     * @var Directory
     *
     * @ORM\ManyToOne(targetEntity="Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="descendant_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $descendant = null;

}
