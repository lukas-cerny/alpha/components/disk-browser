<?php

namespace Alpha\Component\DiskBrowser\Entity;

use Alpha\Utils\Database\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Table(name="component_disk_browser_file", uniqueConstraints={@ORM\UniqueConstraint(name="directory_id", columns={"directory_id", "name"})}, indexes={@ORM\Index(columns={"directory_id"})})
 * @ORM\Entity
 */
class File extends BaseEntity
{
    use MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(name="unique_name", type="string", length=128, nullable=false, unique=true)
     */
    protected $uName;
    /**
     * @var string
     *
     * @ORM\Column(name="stored_name", type="string", length=128, nullable=false, unique=true)
     */
    protected $storedName;
    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", nullable=false)
     */
    protected $mimeType;
    /**
     * @var Directory
     *
     * @ORM\ManyToOne(targetEntity="Directory", inversedBy="files")
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $directory;

    public function isImage()
    {
        return in_array($this->getMimeType(), ['image/png', 'image/jpeg'], true);
    }

    public function isOffice()
    {
        return in_array($this->type, ['application/excel', 'application/vnd.ms-excel', 'application/x-excel', 'application/x-msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
    }

    public function getFileName()
    {
        return $this->parseName()[0];
    }

    public function getExtension()
    {
        return $this->parseName()[1];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUName()
    {
        return $this->uName;
    }

    /**
     * @param string $uName
     */
    public function setUName($uName)
    {
        $this->uName = $uName;
    }

    /**
     * @return string
     */
    public function getStoredName()
    {
        return $this->storedName;
    }

    /**
     * @param string $storedName
     */
    public function setStoredName($storedName)
    {
        $this->storedName = $storedName;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $type
     */
    public function setMimeType($type)
    {
        $this->mimeType = $type;
    }

    /**
     * @return Directory
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param Directory $directory
     */
    public function setDirectory(Directory $directory)
    {
        $this->directory = $directory;
        $directory->addFile($this);
    }

    private function parseName()
    {
        $parts = explode('.', $this->name);
        $result = ['', ''];
        if (count($parts) >= 1) {
            $result[0] = array_shift($parts);
            $result[1] = implode('.', $parts);
        }
        return $result;
    }
}