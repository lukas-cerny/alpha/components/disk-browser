<?php

namespace Alpha\Component\DiskBrowser\Facade;

use Alpha\Component\DiskBrowser\Entity;
use Alpha\Extension\SigmaArchive\Entity\Type;
use Alpha\Utils\Database\Facade\BaseFacade;
use Alpha\Utils\Exceptions\InvalidArgumentException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @package Alpha\Component\DiskBrowser
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Directory extends BaseFacade
{
    public function getEntityClass()
    {
        return Entity\Directory::class;
    }

    public function createRootDirectory($name)
    {
        $dir = new Entity\Directory();
        $dir->setRoot(true);
        $dir->name = $name;
        $this->em->persist($dir);
        $tree = new Entity\DirectoryTree();
        $tree->setAncestor($dir)->setDescendant($dir);
        $this->em->persist($tree);
        $this->em->persist($tree);
        $this->em->flush();
        return $dir;
    }

    public function findParent($directoryId)
    {
        $qb = $this->em->getRepository(Entity\DirectoryTree::class)->createQueryBuilder('t');
        $query = $qb->join('t.ancestor', 'd')->addSelect('d')
            ->where($qb->expr()->eq('t.descendant', $directoryId))
            ->andWhere($qb->expr()->eq('t.depth', 1))
            ->getQuery();
        return $query->getArrayResult();
    }

    public function findChildren($directoryId, $criteria = [], $type = AbstractQuery::HYDRATE_ARRAY)
    {
        $qb = $this->em->getRepository(Entity\DirectoryTree::class)->createQueryBuilder('t');
        $qb->join('t.descendant', 'dir')->addSelect('dir')
            ->where($qb->expr()->eq('t.ancestor', (int)$directoryId))
            ->andWhere($qb->expr()->eq('t.depth', 1));
        $interator = 1;
        foreach ($criteria as $col => $value) {
            $qb->andWhere("$col = ?$interator")
                ->setParameter($interator, $value);
            $interator++;
        }
        return $qb->getQuery()
            ->getResult();
    }

    public function getChildDirectory($parentId, $childName)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('dire')
            ->from(Entity\Directory::class, 'dire')
            ->join(Entity\DirectoryTree::class, 'tree', Join::WITH, 'dire.id = tree.descendant')
            ->where($qb->expr()->eq('tree.ancestor', (int)$parentId))
            ->andWhere('tree.depth = 1')
            ->andWhere('dire.name = :child')
            ->setParameter('child', $childName);

        return $qb->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getItemTree($id)
    {
        $query = $this->em->createQuery('
				SELECT d, IDENTITY (tree1.ancestor), IDENTITY  (tree.descendant) FROM ' . Entity\Directory::class . ' d
				LEFT JOIN ' . Entity\DirectoryTree::class . ' tree WITH (d.id = tree.descendant)
				LEFT JOIN ' . Entity\DirectoryTree::class . ' tree1 WITH (tree1.descendant = tree.descendant AND tree1.depth = 1)
				WHERE tree.ancestor = ?1
				');
        $query->setParameter(1, $id);
        $items = $query->getResult();

        $nodes = [];
        foreach ($items as $item) {
            $nodes[$item[2]]['id'] = (int)$item[0]->id;
            $nodes[$item[2]]['text'] = $item[0]->name;
            $nodes[$item[2]]['parentId'] = (int)$item[1];
            $nodes[$item[2]]['nodes'] = [];
        }

        $root = $nodes[$id];
        unset($nodes[$id]);
        self::getChildren($root, $nodes, $id);

        return $root;
    }

    public static function getChildren(&$root, &$nodes, $id)
    {
        $temp = array_filter($nodes, function ($values, $key) use ($id) {
            return $values['parentId'] === (int)$id;
        }, ARRAY_FILTER_USE_BOTH);

        foreach ($temp as $key => $value) {
            unset($nodes[$key]);
            self::getChildren($value, $nodes, $key);
            $root['nodes'][] = $value;
        }
    }

    public function createDirectory(Entity\Directory $parent, Entity\Directory $item, $parentId = null, $createLeaf = true)
    {
        return $this->em->transactional(function () use ($item, $parent, $parentId, $createLeaf) {
            if ($createLeaf === true) {
                $leaf = new Entity\DirectoryTree();
                $leaf->setAncestor($item)->setDescendant($item);
                $this->em->persist($leaf);
                $this->em->flush($leaf);
            }

            $root = $this->findRoot($parent->getId());
            if (!$root) {
                $root = (new Entity\Directory())->setRoot()->setName(md5($parent->name));
                $this->em->persist($root);

                $rootPath = new Entity\DirectoryTree();
                $rootPath->setAncestor($root)->setDescendant($root);
                $this->em->persist($rootPath);
                $this->em->flush($rootPath);
            }
            if ($parentId === null)
                $parentId = $parent->getId();

            $this->createPath($parentId, $item->getId());
            return $item;
        });
    }

    public function findRoot($id)
    {
        $qb = $this->em->getRepository(Entity\DirectoryTree::class)->createQueryBuilder('t');
        $qb->innerJoin('t.ancestor', 'd')->addSelect('d')
            ->where($qb->expr()->eq('d.root', true))
            ->andWhere($qb->expr()->eq('t.descendant', $id));
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function createPath($parentId, $itemId)
    {
        $conn = $this->em->getConnection();
        $sql = <<<SQL
INSERT INTO component_disk_browser_directory_tree (ancestor_id, descendant_id, depth)
SELECT ancestor_id, :descSelect, depth+1 FROM component_disk_browser_directory_tree
WHERE descendant_id = :desc;
SQL;
        return $conn->executeUpdate($sql, ['descSelect' => $itemId, 'desc' => $parentId], ['descSelect' => \Doctrine\DBAL\Types\Type::INTEGER, 'desc' => \Doctrine\DBAL\Types\Type::INTEGER]);
    }

    public function isChild($parentId, $childId)
    {
        $result = $this->em->getRepository(Entity\DirectoryTree::class)->createQueryBuilder('t')
            ->where('t.ancestor = :parent')
            ->andWhere('t.descendant = :child')
            ->setParameter('parent', (int)$parentId)
            ->setParameter('child', (int)$childId)
            ->getQuery()
            ->getOneOrNullResult();

        return $result !== null;
    }

    /**
     * @param integer $childId
     * @return Entity\Directory
     */
    public function getParent($childId)
    {
        $result = $this->getRepository()->createQueryBuilder('dir')
            ->join(Entity\DirectoryTree::class, 't', Join::WITH, 'dir.id = t.ancestor')
            ->where('t.descendant = :child')
            ->andWhere('t.depth = 1')
            ->setParameter('child', (int)$childId)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function getPath($directoryId)
    {
        $qb = $this->em->createQueryBuilder();
        $data = $qb->select(['d', 't.depth'])
            ->from(Entity\Directory::class, 'd')
            ->join(Entity\DirectoryTree::class, 't', Join::WITH, 'd.id = t.ancestor')
            ->where($qb->expr()->eq('t.descendant', (int)$directoryId))
            ->orderBy('d.id')
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($data as $content) {
            $result[$content['depth']] = $content[0];
        }
        return $result;
    }

    public function getByNameAndParentDir($name, $parentId, $throwException = true)
    {
        $qb = $this->em->createQueryBuilder();
        $entity = $qb->select('d')
            ->from(Entity\Directory::class, 'd')
            ->where($qb->expr()->eq('d.parentDirectory', $parentId))
            ->andWhere('d.name LIKE :name')
            ->setParameter('name', "%$name%")
            ->getQuery()
            ->getOneOrNullResult();

        if ($entity === null && $throwException)
            throw new InvalidArgumentException();

        return $entity;
    }

    /**
     * @return Entity\Directory
     */
    public function insert(array $data, $throwException = true)
    {
        $entity = new Entity\Directory();
        $entity->name = $data['name'];

        if (isset($data['root']))
            $entity->setRoot($data['root']);

        $this->em->persist($entity);

        try {
            $this->em->flush();
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw $e;
        }
    }

    public function recalculatePathsForNode($nodeId, array $nodeIds)
    {
        $conn = $this->em->getConnection();
        $sql = <<<SQL
DELETE t1 FROM component_disk_browser_directory_tree t1
JOIN sigma_library_directory_tree t2 USING (descendant_id) WHERE (t2.ancestor_id = :ancestor AND t1.depth > 0);
SQL;
        $conn->executeUpdate($sql, ['ancestor' => $nodeId], ['ancestor' => Type::INTEGER]);
        $this->calculatePaths($nodeId, $nodeIds);
    }

    public function calculatePaths($root, array $nodeIds)
    {
        $order = 0;
        foreach ($nodeIds as $key => $id) {
            if (!is_array($id)) {
                $this->createPath($root, $id, $order);
            } else {
                $this->createPath($root, $key, $order);
                $this->calculatePaths($key, $id);
            }
            $order++;
        }
    }

    public function fileExists($fileName, $directoryId)
    {
        $qb = $this->em->createQueryBuilder();
        return "0" != $qb->select('COUNT(f)')
                ->from(Entity\File::class, 'f')
                ->where($qb->expr()->eq('f.directory', (int)$directoryId))
                ->andWhere('f.name = ?1')
                ->setParameter('1', $fileName)
                ->getQuery()
                ->getSingleScalarResult();
    }
}