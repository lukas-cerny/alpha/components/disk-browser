<?php

namespace Alpha\Component\DiskBrowser\Facade;

use Alpha\Component\DiskBrowser\Entity;
use Alpha\Utils\Database\Facade\BaseFacade;
use Alpha\Utils\Exceptions\InvalidArgumentException;
use Alpha\Utils\Exceptions\InvalidStateException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * @package Alpha\Component\DiskBrowser
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class File extends BaseFacade
{
    public function getEntityClass()
    {
        return Entity\File::class;
    }

    public static function generateList(array $data)
    {
        $result = [];
        foreach ($data as $row) {
            if (is_array($row))
                $result[$row['id']] = $row['name'];
            else
                $result[$row->getId()] = $row->getName();
        }
        return $result;
    }

    /**
     * @param      $uid
     * @param bool $throwExceptions
     * @return Entity\File
     */
    public function getByUId($uid, $throwExceptions = true)
    {
        $entity = $this->getRepository()->findOneBy([
            'uName' => $uid
        ]);

        if ($entity === null && $throwExceptions)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @param integer $directoryId
     * @return Entity\File[]
     */
    public function getFilesByDirectory($directoryId)
    {
        $qb = $this->em->createQueryBuilder();
        return $qb->select('f')
            ->from(Entity\File::class, 'f')
            ->where($qb->expr()->eq('f.directory', (int)$directoryId))
            ->getQuery()
            ->getResult();
    }

    public function insert(array $data, $throwExceptions = true, $autoFlush = false)
    {
        $entity = new Entity\File;

        $entity->setName($data['name'])
            ->setType($data['type'])
            ->setLibraryDirectory($this->em->find(Entity\Directory::class, $data['directoryId']));

        $this->em->persist($entity);
        try {
            if ($autoFlush)
                $this->em->flush();
        } catch (UniqueConstraintViolationException $ex) {
            if ($throwExceptions)
                throw new InvalidArgumentException();
        }
    }
}