<?php

namespace Alpha\Component\DiskBrowser\Fixtures;

use Alpha\Cache\IStorage;
use Alpha\Router\Facade\Url;
use Alpha\Security\Facade\Permission;
use Alpha\Security\Facade\Resource;
use Alpha\Security\Facade\Role;
use Alpha\Utils\Database\IFixture;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @package Fixtures
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class InitData implements IFixture
{
    private $defaultResources = [
        [
            'name' => 'REST - DiskBrowser',
            'resource' => 'ComponentDiskBrowser:REST:Api',
        ],
    ];
    private $defaultUrls = [
        [
            'link' => 'api/disk/file/create',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'createFile',
        ],
        [
            'link' => 'api/disk/file/download',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'readFile',
        ],
        [
            'link' => 'api/disk/file/update',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'updateFile',
        ],
        [
            'link' => 'api/disk/file/delete',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'deleteFile',
        ],
        [
            'link' => 'api/disk/directory/create',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'createDirectory',
        ],
        [
            'link' => 'api/disk/directory/read',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'readDirectory',
        ],
        [
            'link' => 'api/disk/directory/update',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'updateDirectory',
        ],
        [
            'link' => 'api/disk/directory/delete',
            'presenter' => 'ComponentDiskBrowser:REST:Api',
            'action' => 'deleteDirectory',
        ],
    ];

    public function insert(EntityManagerInterface $em, IStorage $storage)
    {
        $facadeUrl = new Url($em);
        foreach ($this->defaultUrls as $url) {
            foreach (['en', 'cs'] as $locale) {
                if (!$facadeUrl->getBy($url['link'], $locale))
                    $facadeUrl->insert($url['link'], $url['presenter'], $locale, $url['action']);
            }
        }

        $facadeResource = new Resource($em, $storage);
        foreach ($this->defaultResources as $resource) {
            if (!$facadeResource->getByResource($resource['resource']))
                $facadeResource->insert($resource['name'], $resource['resource']);
        }

        $facadeRole = new Role($em, $storage);
        $facadePermission = new Permission($em, $storage);
        foreach ($this->defaultResources as $resource) {
            foreach ($facadeRole->getAll() as $role) {
                if (!$facadePermission->getBy($role, $resource['resource']))
                    $facadePermission->insert(true, true, true, true, $role, $resource['resource']);
            }
        }
    }

}