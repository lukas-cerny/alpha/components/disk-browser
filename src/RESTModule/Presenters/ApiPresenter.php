<?php

namespace Alpha\Component\DiskBrowser\RESTModule\Presenters;

use Alpha\Component\DiskBrowser\Entity;
use Alpha\Component\DiskBrowser\Facade\Directory;
use Alpha\Component\DiskBrowser\Facade\File;
use Alpha\Component\SecuredComponents\Authorizator;
use Alpha\Component\SecuredComponents\SecuredComponentTrait;
use Alpha\System\BasePresenter;
use Alpha\System\Components\General\Flash\Control as Flash;
use Alpha\Utils\Arrays;
use Alpha\Utils\Exceptions\InvalidArgumentException;
use Alpha\Utils\Exceptions\InvalidStateException;
use Alpha\Utils\Strings;
use Nette\Application\Responses\FileResponse;
use Nette\Http\IResponse;
use Nette\Http\Response;
use Nette\Utils\Json;

/**
 * @package Alpha\Component\DiskBrowser
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class ApiPresenter extends BasePresenter
{
    use SecuredComponentTrait;
    /** @var Directory @inject */
    public $facadeDirectory;
    /** @var File @inject */
    public $facadeFile;
    /** @var string */
    private $cipher = '';
    /** @var array */
    private $configuration = [
        'resize' => [],
        'path' => DIRECTORY_SEPARATOR
    ];

    protected function startup()
    {
        parent::startup();
        $this->setView('default');
    }

    public function getSavePath()
    {
        return $this->configuration['path'];
    }

    /**
     * @param string $cipher
     */
    public function setCipher($cipher)
    {
        $this->cipher = $cipher;
    }

    public function setPath($path)
    {
        $this->configuration['path'] = $this->presenter->context->parameters['wwwDir'] . DIRECTORY_SEPARATOR . $path;
    }

    public function setResize($resize)
    {
        $template = [
            'last' => null,
            'pre' => null,
            'width' => null,
            'height' => null,
            'quality' => 100,
        ];
        foreach ($resize as $setting) {
            $this->configuration['resize'][] = Arrays::merge($template, $setting);
        }
        $this->configuration['resize'] = $resize;
    }

    public function actionReadDirectory($id)
    {
        $tree = $this->facadeDirectory->getItemTree($id);
        $this->sendJson($tree);
    }

    public function actionCreateDirectory($token)
    {
        $request = $this->getRequest();
        $data = [
            'parentId' => $request->getPost('parent_id'),
            'name' => $request->getPost('text')
        ];
        $response = [
            'status' => 'success'
        ];
        $parent = $this->facadeDirectory->getById($data['parentId'], false);
        if ($parent === null)
            throw new InvalidStateException('Rodičovská složka nenalezena');
        $this->facadeDirectory->createDirectory($parent, (new Entity\Directory())->setName($request->getPost('text')));

        try {
        } catch (InvalidStateException $e) {
            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        } finally {
            $this->sendJson($response);
        }
    }

    public function actionReadFile($uid, $token, $size = null)
    {
        try {
            $data = (object)Json::decode(Strings::decrypt($token, $this->cipher));
            if (!$this->isAllowedOperation(Authorizator::READ, $data->name, true))
                throw new InvalidStateException('Nemáte dostatečná oprávnění pro čtení souboru');

            $this->sendResponse(self::readFile($this->getHttpResponse(), $this->facadeFile, $uid, $size, $this->configuration['path']));
        } catch (InvalidStateException $e) {
            $this->flashMessage($e->getMessage(), Flash::WARNING);
        }
    }

    public function actionUpdateFile($uid, $token, $operation)
    {
        try {
            $data = (object)Json::decode(Strings::decrypt($token, $this->cipher));
            if (!$this->isAllowedOperation(Authorizator::UPDATE, $data->name, true))
                throw new InvalidStateException('Nemáte dostatečná oprávnění pro úpravu souboru');

            self::updateFile($this->getRequest()->getPost(), $this->facadeFile, $this->facadeDirectory, $uid, $operation, $data->directoryId);
            $this->sendJson([
                'status' => 'success'
            ]);
        } catch (InvalidStateException $e) {
            $this->flashMessage($e->getMessage(), Flash::WARNING);
        }
    }

    public function actionDeleteFile($uid, $token)
    {
        try {
            $data = (object)Json::decode(Strings::decrypt($token, $this->cipher));
            if (!$this->isAllowedOperation(Authorizator::DELETE, $data->name, true))
                throw new InvalidStateException('Nemáte dostatečná oprávnění pro smazání souboru');
            $this->sendResponse(self::deleteFile($this->facadeFile, $uid, $data->directory));
        } catch (InvalidStateException $e) {
            $this->flashMessage($e->getMessage(), Flash::WARNING);
        }
    }


    public static function readFile(IResponse $response, File $facade, $uid, $suffix, $savePath)
    {
        $entity = $facade->getByUId($uid, false);
        if ($entity === null) {
            $response->setCode(Response::S404_NOT_FOUND);
            throw new InvalidStateException('Soubor nenalezen');
        } else {
            if ($suffix !== null) {
                $storedName = substr($entity->getStoredName(), 0, strlen($entity->getStoredName()) - strlen($entity->getExtension()) - 1) . '_' . $suffix . '.' . $entity->getExtension();
            } else {
                $storedName = $entity->getStoredName();
            }

            return new FileResponse($savePath . $storedName, $entity->getName(), $entity->getMimeType(), true);
        }
    }

    /**
     * @param IResponse $response
     * @param Directory $facade
     * @param string    $name
     * @param integer   $parentId
     * @throws InvalidArgumentException
     * @return Entity\Directory Inserted row
     */
    public static function createDirectory(IResponse $response, Directory $facade, $name, $parentId)
    {
        $parent = $facade->getById((int)$parentId, false);
        if ($parent === null) {
            $response->setCode(Response::S404_NOT_FOUND);
            throw new InvalidStateException('Rodičovská složka nenalezena');
        }

        if ($facade->getChildDirectory($parentId, $name)) {
            $response->setCode(Response::S400_BAD_REQUEST);
            throw new InvalidStateException('Složka se stejným názvem již existuje');
        }
        return $facade->createDirectory($parent, (new Entity\Directory())->setName($name));
    }

    public static function updateFile(IResponse $response, $data, File $facade, Directory $facadeDirectory, $uid, $operation, $directoryId)
    {
        $file = $facade->getByUId($uid, false);
        if ($file === null) {
            $response->setCode(Response::S404_NOT_FOUND);
            throw new InvalidStateException('Soubor nenalezen');
        }

        if (!$facadeDirectory->isChild($directoryId, $file->directory->getId())) {
            $response->setCode(Response::S404_NOT_FOUND);
            throw new InvalidStateException('Soubor nelze upravovat');
        }

        switch ($operation) {
            case 'rename':
                $file->setName($data->name);
                break;
            default:
                throw new InvalidStateException('Unsupported operation (' . $operation . ').');
                break;
        }
        $facade->updateEntity($file, true, true);
    }

    public static function deleteFile(File $facade, $uid, $directoryId)
    {
        $file = $facade->getByUId($uid, false);
        if ($file === null)
            throw new InvalidStateException('Soubor nenalezen');

        if ($file->directory->getId() !== (int)$directoryId)
            throw new InvalidStateException('Soubor nelze smazat');

        $facade->deleteById($file->id);
    }
}