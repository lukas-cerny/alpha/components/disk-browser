const fs = require('fs');

const src = process.argv[2];
const dest = process.argv[3];

fs.readdir(src, function(_, files) {
    files.forEach(function(file) {
        copyFile( src + file.toString(), dest + 'component.disk-browser.' + file.toString());
    });
});

function copyFile(src, dest) {
    let readStream = fs.createReadStream(src);
    readStream.once('error', function (err) {
        console.log(err);
    });
    readStream.once('end', function() {
        console.log('done copying - ' + src);
    });
    readStream.pipe(fs.createWriteStream(dest));
}