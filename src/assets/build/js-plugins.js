const path = require('path');
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const resolve = require('rollup-plugin-node-resolve');
const date = new Date();

const plugins = [
    babel({
        sourceType: 'module',
        presets: [
            [
                '@babel/env',
                {
                    loose: true,
                    modules: false,
                    exclude: ['transform-typeof-symbol']
                }
            ]
        ],
        exclude: 'node_modules/**', // Only this package
        plugins: [
            '@babel/plugin-transform-object-assign',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-transform-property-mutators',
            '@babel/plugin-transform-reserved-words',
            'babel-plugin-transform-member-expression-literals',
            'babel-plugin-transform-property-literals'
        ],
    }),
    resolve({
        browser: true
    })
];
const source = {
    FileView: path.resolve(__dirname, '../js/file_view.js'),
    TreeView: path.resolve(__dirname, '../js/tree_view.js'),
    Uploader: path.resolve(__dirname, '../js/uploader.js'),
    DiskBrowser: path.resolve(__dirname, '../js/disk_browser.js')
};

function getBanner(pluginName) {
    return '/*\n'
  + ' * Alpha plugins - DiskBrowser.' + pluginName + '\n'
  + ' * Builded: ' + date.toString() + '\n'
  + ' */';
}


function build(plugin) {
    console.log('Building ' + plugin.toString() + ' plugin... ');

    let fileName = plugin.toLowerCase() + '.js';

    rollup.rollup({
        input: source[plugin],
        plugins
    }).then((bundle) => {
        bundle.write({
            banner: getBanner(plugin),
            format: 'iife',
            name: plugin,
            sourcemap: false,
            file: path.resolve(__dirname, '../dist/js/' + fileName)
        });
    }).then(() => {
        console.log('Building ' + plugin.toString() + ' plugin... Done!');
    });
}

Object.keys(source).forEach((plugin) => build(plugin));