/*
 * Alpha plugins - DiskBrowser.DiskBrowser
 * Builded: Sun Oct 21 2018 16:39:07 GMT+0000 (UTC)
 */
var DiskBrowser = (function (exports) {
  'use strict';

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    subClass.__proto__ = superClass;
  }

  var LightBox = function (window, document) {
    var LightBox =
    /*#__PURE__*/
    function () {
      function LightBox(handler, id) {
        this.options = {
          id: id,
          handler: handler,
          order: 0,
          idSuffix: '-lightbox',
          css: {
            footerHeight: 100,
            headerHeight: 32
          }
        };
        this.items = [];
        this.box = {};
        this.init();
      }

      var _proto = LightBox.prototype;

      _proto.init = function init() {
        this._createBox();

        this._changeImage(this.options.handler.dataset.src_large);

        this._initFooter();

        this._updateFooter();
      };

      _proto._updateImage = function _updateImage() {
        this.options.order = (this.items.length + this.options.order) % this.items.length;
        var img = this.items[this.options.order];

        this._changeImage(img.dataset.src_large);
      };

      _proto._changeImage = function _changeImage(url) {
        var content = this._getContent();

        this.box.img = document.createElement('img');
        this.box.img.setAttribute('src', url);
        this.box.img.style['max-height'] = this.maxHeightImg.toString() + 'px';
        this.box.img.style['max-width'] = this.maxWidthImg.toString() + 'px';

        while (content.firstChild) {
          content.removeChild(content.firstChild);
        }

        content.appendChild(this.box.img);
      };

      _proto.close = function close() {
        this.box.box.remove();
      };

      _proto._createBox = function _createBox() {
        var lightbox = document.createElement('div');
        var header = document.createElement('header');
        var body = document.createElement('div');
        var footer = document.createElement('footer');
        lightbox.setAttribute('id', this.getId);
        lightbox.setAttribute('class', 'lightbox');
        footer.style.height = this.options.css.footerHeight;
        header.style.height = this.options.css.headerHeight;
        body.setAttribute('class', 'body');
        body.style.height = '100%';
        var close = document.createElement('span');
        close.setAttribute('class', 'close cursor');
        var self = this;
        close.addEventListener('click', function () {
          self.close();
        });
        close.innerHTML = '&times;';
        header.appendChild(close);
        var content = document.createElement('div');
        content.setAttribute('class', 'content');
        var next = document.createElement('span');
        next.setAttribute('class', 'next fa fa-chevron-right');
        next.addEventListener('click', function () {
          self.options.order++;

          self._updateImage();

          self._updateFooter();
        });
        var prev = document.createElement('span');
        prev.setAttribute('class', 'prev fa fa-chevron-left');
        prev.addEventListener('click', function () {
          self.options.order--;

          self._updateImage();

          self._updateFooter();
        });
        body.appendChild(content);
        body.appendChild(prev);
        body.appendChild(next);
        window.addEventListener('resize', function () {
          self.box.img.style['max-height'] = self.maxHeightImg.toString() + 'px';
          self.box.img.style['max-width'] = self.maxWidthImg.toString() + 'px';
        }); // Add main childs

        lightbox.appendChild(header);
        lightbox.appendChild(body);
        lightbox.appendChild(footer);
        this.box = {
          body: body,
          box: lightbox,
          close: close,
          content: content,
          footer: footer,
          header: header,
          next: next,
          prev: prev
        };
        document.body.appendChild(lightbox);
      };

      _proto._initFooter = function _initFooter() {
        var footer = this._getFooter();

        var images = document.querySelector(this.options.id).querySelectorAll('.lightbox-item');
        var index = 0;
        var self = this;
        images.forEach(function (image) {
          self.items.push(image);
          var img = document.createElement('img');
          img.setAttribute('src', image.dataset.src_small);
          img.setAttribute('data-large', image.dataset.src_large);
          img.setAttribute('data-order', index);
          img.addEventListener('click', function () {
            self.options.order = img.dataset.order;

            self._updateFooter();

            self._changeImage(img.dataset.large);
          });
          footer.appendChild(img);

          if (self.options.handler === image) {
            self.options.order = index;
          }

          index++;
        });
        this.count = index;
      };

      _proto._updateFooter = function _updateFooter() {
        this.options.order = (this.items.length + this.options.order) % this.items.length;

        var footer = this._getFooter();

        var thumbs = footer.querySelectorAll('img');
        var index = 0;
        var self = this;
        thumbs.forEach(function (thumb) {
          if (index === self.options.order) {
            thumb.setAttribute('class', 'active');
          } else {
            thumb.removeAttribute('class');
          }

          index++;
        });
      };

      _proto._getContent = function _getContent() {
        return this.box.content;
      };

      _proto._getFooter = function _getFooter() {
        return this.box.footer;
      };

      _createClass(LightBox, [{
        key: "maxHeightImg",
        get: function get() {
          var styles = window.getComputedStyle(this.box.content, null);
          return this.box.content.offsetHeight - parseInt(styles.paddingTop) - parseInt(styles.paddingBottom);
        }
      }, {
        key: "maxWidthImg",
        get: function get() {
          return this.box.body.offsetWidth - this.box.prev.offsetWidth - this.box.next.offsetWidth;
        }
      }, {
        key: "getId",
        get: function get() {
          return this.options.id + this.options.idSuffix;
        }
      }]);

      return LightBox;
    }();

    document.addEventListener('click', function (event) {
      var target = event.target;
      var parent = event.target.parentElement;

      if (target.hasAttribute('data-toggle') && target.dataset.toggle === 'lightBox') {
        target.LightBox = new LightBox(target, target.datasets.filesId);
      } else if (parent !== null && parent.hasAttribute('data-toggle') && parent.dataset.toggle === 'lightBox') {
        parent.LightBox = new LightBox(parent, parent.dataset.filesId);
      }
    });

    if (typeof window.LightBox === 'undefined') {
      window.LightBox = LightBox;
    }

    return LightBox;
  }(window, document);

  /**
   * Base function for support Nette framework
   * @author Lukáš Černý
   */
  var Nette = function (document, window) {
    var Nette =
    /*#__PURE__*/
    function () {
      function Nette() {}

      Nette.parseResponse = function parseResponse(response) {
        if (response.snippets) {
          Nette.updateSnippets(response.snippets);
        }

        if (response.redirect) {
          Nette.redirect(response.redirect);
        }
      };

      Nette.redirect = function redirect(location) {
        document.location.replace(location);
      };

      Nette.updateSnippets = function updateSnippets(snippets) {
        for (var snippet in snippets) {
          var $el = document.getElementById(snippet);
          updateSnippet($el, snippets[snippet]);
        }

        function updateSnippet($el, html) {
          if ($el.querySelectorAll('title').length) {
            document.title = html;
          } else {
            if ($el.querySelectorAll('[data-ajax-append]').length) {
              $el.append(html);
            } else if ($el.querySelectorAll('[data-ajax-prepend]').length) {
              $el.prepend(html);
            } else {
              $el.innerHTML = html;
            }
          }
        }
      };

      return Nette;
    }();

    if (typeof window.Nette === 'undefined') {
      window.Nette = Nette;
    }

    return Nette;
  }(document, window);

  /**
   * Base functions
   * @author Lukáš Černý
   */

  var Base = function (document, window) {
    var Base =
    /*#__PURE__*/
    function () {
      function Base() {}

      var _proto = Base.prototype;

      _proto.sendRequest = function sendRequest(config) {
        config = _extends({
          data: {},
          type: 'POST',
          async: true,
          contentType: null,
          success: undefined,
          error: undefined,
          ajax: true
        }, config);
        var xhr;

        if (window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        } else {
          xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
        }

        xhr.open(config.type, config.destination, config.async);

        if (config.contentType) {
          xhr.setRequestHeader('Content-Type', config.contentType);
        }

        if (config.ajax) {
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }

        xhr.onload = function () {
          if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            Nette.parseResponse(response);

            if (config.success !== undefined) {
              config.success(xhr.responseText);
            }
          } else {
            if (config.error !== undefined) {
              config.error(xhr.responseText);
            }
          }
        };

        if (config.type === 'POST') {
          var data = '';
          if (config.contentType === 'application/json') data = JSON.stringify(config.data);else data = config.data;
          xhr.send(data);
        } else {
          xhr.send();
        }
      };

      _proto.addEventListener = function addEventListener(invokedOn, name, callback, context) {
        if (typeof callback !== 'function') {
          return;
        }

        if (document.addEventListener) {
          invokedOn.addEventListener(name, function (event) {
            callback.apply(context, arguments);
            event.preventDefault();
            return false;
          }, false);
        } else {
          invokedOn.attachEvents('on' + name, function () {
            callback.apply(context, arguments);
            window.event.returnValue = false;
          }, false);
        }
      };

      _proto.removeEventListener = function removeEventListener(invokedOn, name, callback, context) {
        if (typeof callback !== 'function') {
          return;
        }

        if (document.removeEventListener) {
          invokedOn.removeEventListener(name, callback.bind(context), false);
        } else {
          invokedOn.attachEvents('on' + name, function () {
            callback.apply(context, arguments);
            window.event.returnValue = false;
          }, false);
        }
      };

      return Base;
    }();

    return Base;
  }(document, window);

  /**
   * Modal widnow for Bootstrap 4
   * @author Lukáš Černý
   */
  var Modal = function (document, window) {

    var EVENTS = {
      HIDE: 'hide',
      HIDE_PRE: 'pre_hide'
    };

    var Modal =
    /*#__PURE__*/
    function () {
      function Modal(id, content, config) {
        this._modal = undefined;
        this._backdrop = undefined;
        config = _extends({
          texts: {},
          config: {},
          elements: {}
        }, config);
        this._config = {
          id: id,
          class: 'modal fade',
          content: content
        };
        this._options = {
          texts: _extends({
            title: 'TITLE',
            closeButton: 'Close'
          }, config.texts),
          config: _extends({
            closeButton: true,
            footerButtons: []
          }, config.config),
          elements: _extends({
            title: 'h4'
          }, config.elements)
        };

        this._generateModal();

        this._generateBackdrop();
      }

      var _proto = Modal.prototype;

      _proto.show = function show() {
        if (typeof this._modal !== 'undefined') {
          document.body.classList.add('open-modal');
          var toShow = this.modal;

          if (!toShow.parentNode || toShow.parentNode.nodeType !== Node.ELEMENT_NODE) {
            document.body.appendChild(toShow);
            document.body.appendChild(this._backdrop);
          }

          toShow.style.display = 'block';
          toShow.classList.add('show');

          this._backdrop.classList.add('show');
        }
      };

      _proto.hide = function hide(event) {
        if (event) {
          event.preventDefault();
        }

        if (typeof this._modal !== 'undefined') {
          var toHide = this.modal;
          toHide.style.display = 'none';
          this._backdrop.style.display = 'none';
          toHide.setAttribute('aria-hidden', true);

          this._backdrop.setAttribute('aria-hidden', true);

          document.body.classList.remove('modal-open');
          var newEvent = document.createEvent('Event');
          newEvent.initEvent(Modal.eventHide, true, true);
          toHide.dispatchEvent(newEvent);
          toHide.remove();

          this._backdrop.remove();
        }
      };

      _proto._generateModal = function _generateModal() {
        var structure = {
          title: document.createElement(this._options.elements.title),
          header: document.createElement('div'),
          body: document.createElement('div'),
          footer: document.createElement('div'),
          content: document.createElement('div'),
          dialog: document.createElement('div')
        };

        for (var key in structure) {
          structure[key].className = 'modal-' + key;
        }

        var that = this;
        structure.title.innerText = this._options.texts.title;
        structure.header.appendChild(structure.title);
        var closeButton = document.createElement('button');
        closeButton.setAttribute('type', 'button');
        closeButton.setAttribute('class', 'close');
        closeButton.dataset.dismiss = 'modal';
        closeButton.innerHTML = '&times;';
        closeButton.addEventListener('click', function () {
          this.hide();
        }.bind(this));
        structure.header.appendChild(closeButton);

        if (typeof this._config.content === 'string') {
          structure.body.innerHTML = this._config.content;
        } else {
          structure.body.appendChild(this._config.content);
        }

        structure.content.appendChild(structure.header);
        structure.content.appendChild(structure.body);
        structure.content.appendChild(structure.footer);
        structure.dialog.appendChild(structure.content);
        structure.modal = document.createElement('div');
        structure.modal.className = this._config.class;
        structure.modal.setAttribute('id', this._config.id);
        structure.modal.appendChild(structure.dialog);

        this._options.config.footerButtons.forEach(function (button) {
          structure.footer.appendChild(button);
        });

        if (this._options.config.closeButton) {
          var closeButtonFooter = document.createElement('button');
          closeButtonFooter.setAttribute('type', 'button');
          closeButtonFooter.className = 'btn btn-default';
          closeButtonFooter.dataset.dismiss = 'modal';
          closeButtonFooter.innerHTML = this._options.texts.closeButton;
          closeButtonFooter.addEventListener('click', function () {
            that.hide();
          });
          structure.footer.appendChild(closeButtonFooter);
        }

        this._modal = structure;
      };

      _proto._generateBackdrop = function _generateBackdrop() {
        this._backdrop = document.createElement('div');
        this._backdrop.className = 'modal-backdrop fade';
      };

      _createClass(Modal, [{
        key: "modal",
        get: function get() {
          return this._modal.modal;
        }
      }], [{
        key: "eventHide",
        get: function get() {
          return EVENTS.HIDE;
        }
      }, {
        key: "eventHidePre",
        get: function get() {
          return EVENTS.HIDE_PRE;
        }
      }]);

      return Modal;
    }();

    if (typeof window.Modal === 'undefined') {
      window.Modal = Modal;
    }

    return Modal;
  }(document, window);

  var ContextMenu = function (document, window) {
    var ContextMenu =
    /*#__PURE__*/
    function (_Base) {
      _inheritsLoose(ContextMenu, _Base);

      function ContextMenu(invokedOn, config) {
        var _this;

        _this = _Base.call(this) || this;
        _this.position = _extends({
          left: 0,
          top: 0
        }, config.position);
        _this.items = _extends([], config.items);
        _this.invokedOn = invokedOn;
        _this.contextMenu = undefined;
        _this.attached = false;

        _this._init();

        return _this;
      }

      var _proto = ContextMenu.prototype;

      _proto._init = function _init() {
        var ul = this._getTemplate('list');

        this.contextMenu = ul;
        ul.setAttribute('role', 'menu');
        ul.style.display = 'none';
        this.items.forEach(function (action) {
          var item;

          if (action.type === 'divider') {
            item = this._getTemplate('divider');
          } else {
            item = this._getTemplate('item');

            var text = this._getTemplate('innerText'),
                span = this._getTemplate('icon');

            if (action.class) {
              action.class.split(' ').forEach(function (className) {
                item.classList.add(className);
              });
            }

            text.innerText = action.text;

            if (action.icon) {
              action.icon.split(' ').forEach(function (className) {
                span.classList.add(className);
              });
            }

            item.appendChild(span);
            item.appendChild(text);
            this.addEventListener(item, 'click', function (e) {
              if (action.callback) {
                action.callback.apply(this, [this.invokedOn, action.id]);
              }

              this._clickHandler(e);
            }.bind(this), this);
          }

          ul.appendChild(item);
        }.bind(this));
        this.addEventListener(this.contextMenu, 'contextmenu', function () {});
      };

      _proto._getTemplate = function _getTemplate(type) {
        var template = {
          list: {
            elem: 'ul',
            class: 'context-menu dropdown-menu'
          },
          item: {
            elem: 'li',
            class: 'dropdown-item'
          },
          divider: {
            elem: 'li',
            class: 'dropdown-divider'
          },
          icon: {
            elem: 'span',
            class: 'icon'
          },
          innerText: {
            elem: 'span',
            class: ''
          }
        };
        var elem = document.createElement(template[type].elem);
        elem.className = template[type].class;
        return elem;
      };

      _proto._clickHandler = function _clickHandler(event) {
        this.destroy();
      };

      _proto.show = function show() {
        if (this.attached) return;
        document.body.appendChild(this.contextMenu);
        this.contextMenu.style.display = 'block';
        this.contextMenu.style.position = 'absolute';
        this.contextMenu.style.left = this.position.left + 'px';
        this.contextMenu.style.top = this.position.top + 'px';
        this.attached = true;
        this.addEventListener(document, 'click', this.destroy, this);
      };

      _proto.hide = function hide() {
        if (!this.attached) return;
        this.removeEventListener(document, 'click', this.destroy, this);
        this.contextMenu.style.display = 'none';
        this.contextMenu.remove();
        this.attached = false;
      };

      _proto.destroy = function destroy() {
        this.hide();
        this.contextMenu = undefined;
      };

      return ContextMenu;
    }(Base);

    if (typeof window.ContextMenu === 'undefined') {
      window.ContextMenu = ContextMenu;
    }

    return ContextMenu;
  }(document, window);

  var TreeView = function (window, document) {
    var TreeView =
    /*#__PURE__*/
    function (_Base) {
      _inheritsLoose(TreeView, _Base);

      function TreeView(element, config) {
        var _this;

        _this = _Base.call(this) || this;
        _this.rootInitialized = false;
        _this.wrapper = undefined;
        _this.structure = {};
        _this.nodes = {};
        _this.invokedOn = element;
        _this.contextMenu = undefined;
        _this.options = {
          ajax: _extends({}, config.ajax),
          config: _extends({
            rootId: -1,
            addRoot: false,
            rootName: '/',
            debug: true,
            highlightSelected: true,
            showBorder: true,
            showHover: true,
            sort: true
          }, config.config || {}),
          contextMenu: _extends({
            enabled: true,
            items: [],
            removeTitle: 'Delete node',
            removeMessage: 'Do you really want to remove:',
            buttonYes: 'Yes',
            buttonNo: 'No'
          }, config.contextMenu || {}),
          icons: _extends({
            expand: 'fa fa-minus',
            collapse: 'fa fa-plus',
            empty: 'fa',
            node: '',
            selected: '',
            sendInputForm: 'fa fa-check',
            cancelInputForm: 'fa fa-times'
          }, config.icons || {}),
          error: _extends({
            title: 'Error!!!',
            button: 'OK'
          }, config.error || {}),
          events: _extends({
            onNodeCollapsed: undefined,
            onNodeExpanded: undefined,
            onNodeSelected: undefined,
            onNodeUnselected: undefined
          }, config.events || {}),
          initStates: _extends({
            expanded: false
          }, config.initStates || {})
        };

        _this.init();

        return _this;
      }

      var _proto = TreeView.prototype;

      _proto.init = function init(data) {
        if (data === undefined) {
          if (!this.options.data && typeof this.options.ajax.root === 'string') {
            this.sendRequest({
              destination: this.options.ajax.root,
              success: function (response) {
                this.init(response);
              }.bind(this)
            });
          } else {
            this.options.data = this.options.data || [];
            this.init(this.options.data);
          }
        } else {
          if (typeof data === 'string') {
            data = JSON.parse(data);
          }

          if (this.options.data) {
            delete this.options.data;
          }

          if (this.options.config.addRoot || !data.hasOwnProperty('id')) {
            this.rootInitialized = true;

            this._initNode({
              id: this.options.config.rootId,
              text: this.options.config.rootName,
              selectable: false,
              states: {
                expanded: true
              },
              nodes: []
            }, undefined);
          }

          if (data.hasOwnProperty('id')) {
            this._initNode(data, undefined, 0);
          } else {
            this._initNodes(data, this.options.config.rootId, 0);
          }

          this._eventsSubscribe();

          this.render();
        }
      };

      _proto._getTemplate = function _getTemplate(type) {
        var template = {
          list: {
            elem: 'ul',
            class: 'list-group'
          },
          item: {
            elem: 'li',
            class: 'list-group-item'
          },
          indent: {
            elem: 'span',
            class: 'indent'
          },
          icon: {
            elem: 'span',
            class: 'icon'
          },
          innerText: {
            elem: 'span',
            class: ''
          }
        };
        var elem = document.createElement(template[type].elem);
        elem.setAttribute('class', template[type].class);
        return elem;
      };

      _proto._initNode = function _initNode(node, parentId, indent) {
        if (!node) return undefined;
        var item = this.getNode(node.id) || {
          id: node.id,
          parentId: parentId,
          text: node.text,
          selectable: node.selectable !== undefined ? node.selectable : true,
          indent: indent,
          colors: node.colors || {},
          nodes: [],
          states: node.states || {
            selected: false
          }
        };

        if (!item.states.hasOwnProperty('expanded')) {
          if (node.nodes && node.nodes.length > 0) {
            item.states.expanded = this.options.initStates.expanded;
          } else {
            item.states.expanded = false;
          }
        }

        this.nodes[item.id] = item;

        if (parentId) {
          var parent = this.getNode(parentId);
          parent.nodes.push(item);

          this._sortNodes(parent);
        } else if (this.options.config.addRoot && item.id !== this.options.config.rootId) {
          item.parentId = this.options.config.rootId;
          this.getNode(this.options.config.rootId).nodes.push(item);
        } else {
          this.structure = item;
        }

        this._buildNode(item);

        this._sortNodes(item);

        this._initNodes(node.nodes, item.id, indent + 1);

        return item;
      };

      _proto._initNodes = function _initNodes(nodes, parentId, indent) {
        if (!Array.isArray(nodes) || nodes.length === 0) {
          return;
        }

        nodes.forEach(function (node) {
          this._initNode(node, parentId, indent);
        }.bind(this));
      };

      _proto._updateNode = function _updateNode(node) {
        if (!node) return;
        var item = node.element;

        if (node.states.selected) {
          item.classList.add('selected');
          item.classList.remove('hover');
        } else {
          item.classList.remove('selected');

          if (this.options.config.showHover) {
            item.classList.add('hover');
          }
        }

        node.elementText.innerText = node.text;
        var parent = this.getNode(node.parentId);

        if (parent && !parent.states.expanded) {
          item.setAttribute('hidden', true);
        } else if (parent && parent.states.expanded) {
          item.removeAttribute('hidden');
        } else if (!parent && !this.options.config.addRoot && this.rootInitialized) {
          item.setAttribute('hidden', true);
        }

        var icon = node.elementIcon;
        var add,
            remove = [];

        if (node.nodes) {
          if (node.states.expanded) {
            add = this.options.icons.expand.split(' ');
            remove = this.options.icons.collapse.split(' ');
          } else {
            remove = this.options.icons.expand.split(' ');
            add = this.options.icons.collapse.split(' ');
          }
        }

        remove.forEach(function (className) {
          icon.classList.remove(className);
        });
        add.forEach(function (className) {
          icon.classList.add(className);
        });
      };

      _proto._buildNode = function _buildNode(node) {
        var item = this._getTemplate('item');

        var icon = this._getTemplate('icon');

        var text = this._getTemplate('innerText');

        node.element = item;
        node.elementIcon = icon;
        node.elementText = text;
        item.setAttribute('data-id', node.id);
        item.appendChild(icon);
        item.appendChild(text);

        this._updateNode(node);
      };

      _proto._sortNodes = function _sortNodes(node) {
        if (this.options.config.sort) {
          node.nodes = node.nodes.sort(function (a, b) {
            if (a.text === b.text) {
              return a.id < b.id;
            } else {
              return a.text > b.text;
            }
          });
        }
      };

      _proto.render = function render() {
        var el = this.invokedOn;

        while (el.firstChild) {
          el.removeChild(el.firstChild);
        }

        el.classList.add('treeView');
        this.wrapper = this._getTemplate('list');
        el.appendChild(this.wrapper);

        this._buildTree(this.structure, 0);
      };

      _proto._buildTree = function _buildTree(node) {
        if (!node) return;
        var icon = node.elementIcon,
            parent = this.getNode(node.parentId);
        var indent = 0;

        if (node.parentId) {
          if (!(node.parentId === this.options.config.rootId && !this.options.config.addRoot && this.rootInitialized)) {
            indent = parent.indent + 1;
          }
        }

        node.indent = indent;

        for (var i = 0; i < indent; i++) {
          icon.before(this._getTemplate('indent'));
        }

        this._sortNodes(node);

        if (node.parentId === undefined || node.parentId === this.structure.id) {
          this.wrapper.appendChild(node.element);
        } else {
          for (var _i = 0; _i < parent.nodes.length; _i++) {
            var tempNode = parent.nodes[_i];

            if (tempNode.id === node.id) {
              if (_i > 0) {
                parent.nodes[_i - 1].element.after(node.element);
              } else {
                parent.element.after(node.element);
              }

              break;
            }
          }
        }

        node.nodes.forEach(this._buildTree.bind(this));
      };

      _proto._eventsSubscribe = function _eventsSubscribe() {
        this.addEventListener(this.invokedOn, 'click', this._clickHandler, this);
        this.addEventListener(this.invokedOn, 'nodeExpanded', this.options.events.onNodeExpanded, this);
        this.addEventListener(this.invokedOn, 'nodeCollapsed', this.options.events.onNodeCollapsed, this);
        this.addEventListener(this.invokedOn, 'nodeSelected', this.options.events.onNodeSelected, this);
        this.addEventListener(this.invokedOn, 'nodeUnselected', this.options.events.onNodeUnselected, this);

        if (this.options.contextMenu.enabled) {
          this.addEventListener(this.invokedOn, 'contextmenu', this._contextMenu, this);
        } else {
          this.addEventListener(this.invokedOn, 'contextmenu', function () {});
        }
      };

      _proto._eventsUnsubscribe = function _eventsUnsubscribe() {
        this.removeEventListener(this.invokedOn, 'nodeExpanded', this.options.events.onNodeExpanded, this);
        this.removeEventListener(this.invokedOn, 'nodeCollapsed', this.options.events.onNodeCollapsed, this);
        this.removeEventListener(this.invokedOn, 'nodeSelected', this.options.events.onNodeSelected, this);
        this.removeEventListener(this.invokedOn, 'nodeUnselected', this.options.events.onNodeUnselected, this);

        if (this.options.contextMenu.enabled) {
          this.removeEventListener(this.invokedOn, 'contextmenu', this._contextMenu, this);
          this.removeEventListener(document, 'click', this._contextMenuHide, this);
        } else {
          this.removeEventListener(this.invokedOn, 'contextmenu', function () {
            return false;
          }, this);
        }
      };

      _proto._clickHandler = function _clickHandler(event) {
        var target = event.target;
        var tag = target.tagName;
        if (tag === 'input' || tag === 'button' || target.parentElement.tagName === 'button') return;
        var node = this.findNode(target),
            classList = target.classList;
        if (!node) return;

        if (classList.contains('icon')) {
          this._toggleExpandedState(node);
        } else {
          if (node.selectable) {
            this._toggleSelectedState(node);
          } else {
            this._toggleExpandedState(node);
          }
        }
      };

      _proto._toggleExpandedState = function _toggleExpandedState(node) {
        if (!node) return;

        this._setExpandedState(node, !node.states.expanded);
      };

      _proto._toggleSelectedState = function _toggleSelectedState(node) {
        if (!node) return;

        this._setSelectedState(node, !node.states.selected);
      };

      _proto._setExpandedState = function _setExpandedState(node, state) {
        if (state && state === node.states.expanded) return;

        if (state && node.nodes) {
          node.states.expanded = true;
          node.nodes.forEach(function (subNode) {
            this._updateNode(subNode);
          }.bind(this));
          var event = new CustomEvent('nodeExpanded', {
            detail: node
          });
          this.invokedOn.dispatchEvent(event);
        } else if (!state) {
          node.states.expanded = false;

          var _event = new CustomEvent('nodeCollapsed', {
            detail: node
          });

          this.invokedOn.dispatchEvent(_event);

          if (node.nodes) {
            node.nodes.forEach(function (subNode) {
              this._setExpandedState(subNode, false);
            }.bind(this));
          }
        }

        this._updateNode(node);
      };

      _proto._setSelectedState = function _setSelectedState(node, state) {
        if (state === node.states.selected) return;
        var event;

        if (state) {
          this.getSelected().forEach(function (subNode) {
            this._setSelectedState(subNode, false);
          }.bind(this));
          event = new CustomEvent('nodeSelected', {
            detail: node
          });
        } else {
          event = new CustomEvent('nodeUnselected', {
            detail: node
          });
        }

        this.invokedOn.dispatchEvent(event);
        node.states.selected = state;

        this._updateNode(node);
      };

      _proto._findNodes = function _findNodes(pattern, modifier, attribute) {
        modifier = modifier || 'g';
        attribute = attribute || 'text';
        var values = Object.keys(this.nodes).map(function (key) {
          return this.nodes[key];
        }.bind(this));
        return values.filter(function (node) {
          var val = this._getNodeValue(node, attribute);

          if (typeof val === 'string') {
            return val.match(new RegExp(pattern, modifier));
          }
        }.bind(this));
      };

      _proto._getNodeValue = function _getNodeValue(obj, attr) {
        var index = attr.indexOf('.');

        if (index > 0) {
          var _obj = obj[attr.substring(0, index)];

          var _attr = attr.substring(index + 1, attr.length);

          return this._getNodeValue(_obj, _attr);
        } else {
          if (obj.hasOwnProperty(attr)) {
            return obj[attr].toString();
          } else {
            return undefined;
          }
        }
      };

      _proto.getNode = function getNode(id) {
        if (this.nodes.hasOwnProperty(id)) {
          return this.nodes[id];
        } else {
          return undefined;
        }
      };

      _proto.findNode = function findNode(target) {
        var closest = target.closest('li.list-group-item');

        if (!closest) {
          return undefined;
        }

        var id = closest.dataset.id,
            node = this.getNode(id);

        if (!node) {
          this._logError('Error: node does not exists');
        }

        return node;
      };

      _proto._destroy = function _destroy() {
        if (this.initialized) {
          this.wrapper.remove();
          this.wrapper = null;

          this._eventsUnsubscribe();

          if (this.contextMenu) {
            this.contextMenu.destroy();
            this.contextMenu = undefined;
          }

          this.initialized = false;
        }
      };

      _proto._contextMenu = function _contextMenu(event) {
        var target = event.target;
        var node = this.findNode(target);
        if (!node) return;
        this.options.contextMenu.items.forEach(function (item) {
          var callback = item.callback;

          if (item.id === 'create') {
            item.callback = function (invokedOn) {
              var data = this._contextMenuInputNode(),
                  modal = new Modal(this.elementId + '-add', data.input, {
                texts: {
                  title: item.text
                },
                config: {
                  footerButtons: data.buttons
                }
              });

              data.buttons.forEach(function (button) {
                button.addEventListener('click', function (event) {
                  var target = event.target;

                  if (target.tagName === 'span') {
                    target = target.parentNode;
                  }

                  if (data.input.value !== '') {
                    modal.hide();
                    this.node({
                      type: 'create',
                      parentId: invokedOn.id,
                      text: data.input.value,
                      callback: callback
                    });
                  }
                }.bind(this));
              }.bind(this));
              modal.show();
            }.bind(this);
          } else if (item.id === 'rename') {
            item.callback = function (invokedOn) {
              var data = this._contextMenuInputNode(),
                  input = data.input.value = invokedOn.text,
                  modal = new Modal(this.elementId + '-rename', data.input, {
                texts: {
                  title: item.text
                },
                config: {
                  footerButtons: data.buttons
                }
              });

              data.buttons.forEach(function (button) {
                button.addEventListener('click', function (event) {
                  var target = event.target;

                  if (target.tagName === 'span') {
                    target = target.parentNode;
                  }

                  if (input.value !== invokedOn.text) {
                    modal.hide();
                    this.node({
                      type: 'rename',
                      id: invokedOn.id,
                      text: data.input.value,
                      callback: callback
                    });
                  }
                }.bind(this));
              }.bind(this));
              modal.show();
            }.bind(this);
          } else if (item.id === 'delete') {
            item.callback = function (invokedOn) {
              if (!invokedOn) return;

              var data = this._contextMenuRemoveModal(invokedOn);

              data.button.addEventListener('click', function () {
                data.modal.hide();
                this.node({
                  type: 'delete',
                  id: invokedOn.id,
                  callback: callback
                });
              }.bind(this));
              data.modal.show();
            }.bind(this);
          }
        }.bind(this));

        if (this.contextMenu) {
          this.contextMenu.destroy();
        }

        this.contextMenu = new ContextMenu(node, {
          items: this.options.contextMenu.items,
          position: {
            left: event.pageX,
            top: event.pageY
          }
        });
        this.contextMenu.show();
      };

      _proto.node = function node(data) {
        var config = {
          data: new FormData(),
          error: function (data) {
            var response = JSON.parse(data),
                modal = this._displayErrorModal(response.message);

            modal.show();
          }.bind(this)
        };

        if (data.type === 'create') {
          config.destination = this.options.ajax.create;
          config.data.append('parent_id', data.parentId);
          config.data.append('text', data.text);

          config.success = function (response) {
            response = JSON.parse(response);
            var parent = this.getNode(data.parentId),
                node = {
              id: response.id,
              text: data.text,
              parentId: data.parentId
            };
            node = this._initNode(node, data.parentId);

            this._buildTree(node);

            this._setExpandedState(parent, true);

            this._setSelectedState(node, true);
          }.bind(this);
        } else if (data.type === 'rename') {
          config.destination = this.options.ajax.update;
          config.data.append('id', data.id.toString());
          config.data.append('text', data.text);

          config.success = function () {
            var node = this.getNode(data.id);
            node.text = data.text;

            this._updateNode(node);
          }.bind(this);
        } else if (data.type === 'delete') {
          config.destination = this.options.ajax.delete;
          config.data.append('id', data.id);

          config.success = function () {
            var node = this.getNode(data.id);

            this._nodeDelete(node);
          }.bind(this);
        }

        this.sendRequest(config);

        if (data.callback) {
          data.callback();
        }
      };

      _proto._nodeDelete = function _nodeDelete(node) {
        if (!node) {
          return;
        }

        if (node.nodes && node.nodes.length > 0) {
          node.nodes.forEach(this._nodeDelete.bind(this));
        }

        node.element.remove();
        delete this.nodes[node.id];

        if (node.parentId !== undefined) {
          var parent = this.getNode(node.parentId);
          parent.nodes = parent.nodes.filter(function (value) {
            return value.id !== node.id;
          });
        }
      };

      _proto._contextMenuInputNode = function _contextMenuInputNode() {
        var btn = function btn(name, className, spanClass) {
          var btn = document.createElement('button'),
              span = document.createElement('span');
          btn.setAttribute('type', 'button');
          btn.setAttribute('name', name);
          btn.setAttribute('class', 'btn ' + className);
          span.setAttribute('class', spanClass);
          btn.appendChild(span);
          return btn;
        };

        var input = document.createElement('input');
        input.setAttribute('class', 'form-control');
        input.setAttribute('type', 'text');
        input.setAttribute('name', 'name');
        return {
          input: input,
          buttons: [btn('send', 'btn-success', this.options.icons.sendInputForm)]
        };
      };

      _proto._contextMenuRemoveModal = function _contextMenuRemoveModal(node) {
        var btn = document.createElement('button');
        btn.setAttribute('type', 'submit');
        btn.setAttribute('name', 'yes');
        btn.setAttribute('class', 'btn btn-success');
        btn.innerText = this.options.contextMenu.buttonYes;
        return {
          modal: new Modal(this.elementId + 'edit', this.options.contextMenu.removeMessage + ' ' + node.text, {
            texts: {
              title: this.options.contextMenu.removeTitle,
              closeButton: this.options.contextMenu.buttonNo
            },
            config: {
              footerButtons: [btn]
            }
          }),
          button: btn
        };
      };

      _proto._displayErrorModal = function _displayErrorModal(message) {
        var content = document.createElement('div');
        content.innerHTML = message;
        return new Modal(this.elementId, content, {
          texts: {
            title: this.options.error.title,
            closeButton: this.options.error.button
          }
        });
      };

      _proto._getParent = function _getParent(identifier) {
        var node = this._identifyNode(identifier);

        return this.getNode(node.parentId);
      };

      _proto._expandLevels = function _expandLevels(nodes) {
        nodes.forEach(function (node) {
          this._setExpandedState(node, true);

          if (node.nodes) {
            this._expandLevels(node.nodes);
          }
        }.bind(this));
      };

      _proto._forEachIdentifier = function _forEachIdentifier(identifiers, callback) {
        if (!(identifiers instanceof Array)) {
          identifiers = [identifiers];
        }

        identifiers.forEach(function (identifier) {
          callback(this._identifyNode(identifier));
        }.bind(this));
      };

      _proto._identifyNode = function _identifyNode(identifier) {
        if (typeof identifier === 'number') {
          return this.getNode(identifier);
        } else {
          return identifier;
        }
      };

      _proto.remove = function remove() {
        this._destroy();
      };

      _proto.toggleNodeExpanded = function toggleNodeExpanded(identifiers) {
        this._forEachIdentifier(identifiers, this._toggleExpandedState.bind(this));
      };

      _proto.toggleNodeSelected = function toggleNodeSelected(identifiers) {
        this._forEachIdentifier(identifiers, this._toggleSelectedState.bind(this));
      };

      _proto.selectNode = function selectNode(identifiers) {
        this._forEachIdentifier(identifiers, function (node) {
          this._setSelectedState(node, true);
        }.bind(this));
      };

      _proto.unselectNode = function unselectNode(identifiers) {
        this._forEachIdentifier(identifiers, function (node) {
          this._setSelectedState(node, false);
        }.bind(this));
      };

      _proto.collapseAll = function collapseAll() {
        this._forEachIdentifier(this.getExpanded(), function (node) {
          this._setExpandedState(node, false);
        }.bind(this));
      };

      _proto.expandAll = function expandAll() {
        this._forEachIdentifier(this.getCollapsed(), function (node) {
          this._setSelectedState(node, true);
        }.bind(this));
      };

      _proto.getSelected = function getSelected() {
        return this._findNodes('true', 'g', 'states.selected');
      };

      _proto.getUnselected = function getUnselected() {
        return this._findNodes('false', 'g', 'states.selected');
      };

      _proto.getExpanded = function getExpanded() {
        return this._findNodes('true', 'g', 'states.expanded');
      };

      _proto.getCollapsed = function getCollapsed() {
        return this._findNodes('false', 'g', 'states.expanded');
      };

      return TreeView;
    }(Base);

    if (typeof window.TreeView === 'undefined') {
      window.TreeView = TreeView;
    }

    return TreeView;
  }(window, document);

  var Uploader = function (window, document) {
    var Uploader =
    /*#__PURE__*/
    function () {
      function Uploader(element) {
        this._options = {
          id: null,
          srcElement: element,
          multiple: true,
          popup: true,
          accept: null
        };
        this._uploadRun = false;
        this._uploadFileIndex = 0;
        this._files = [];
        this._modal = undefined;
        this._input = undefined;

        this._init();
      }

      var _proto = Uploader.prototype;

      _proto._init = function _init() {
        this._generateForm();

        this._modal = new Modal(this.id + '-modal', this._input.container, {
          texts: {
            title: this.title,
            closeButton: this.closeButton
          }
        });

        this._modal.show();
      };

      _proto._generateForm = function _generateForm() {
        var structure = {
          container: document.createElement('div'),
          inputDiv: document.createElement('div'),
          input: document.createElement('input'),
          label: document.createElement('label'),
          files: document.createElement('div')
        };
        structure.inputDiv.className = 'custom-file row';
        structure.container.className = 'upload';
        structure.files.className = 'files row';
        structure.label.className = 'custom-file-label';
        structure.input.className = 'custom-file-input';
        structure.label.setAttribute('for', this.id + '-input');
        structure.input.setAttribute('id', this.id + '-input');
        structure.input.setAttribute('type', 'file');

        if (this.isMultiple) {
          structure.input.setAttribute('multiple', '');
        }

        if (this.accept !== null) {
          structure.input.setAttribute('accept', this.accept);
        }

        var that = this;
        structure.input.addEventListener('change', function (event) {
          event.preventDefault();
          that.inputChange();
        });
        structure.files.addEventListener('add', function (event) {
          event.preventDefault();

          that._startUpload();
        });
        structure.inputDiv.appendChild(structure.input);
        structure.inputDiv.appendChild(structure.label);
        structure.container.appendChild(structure.inputDiv);
        structure.container.appendChild(structure.files);
        this._input = structure;
      };

      _proto.inputChange = function inputChange() {
        var input = this._input.input;

        for (var i = 0; i < input.files.length; i++) {
          var file = input.files[i];
          this.addFile(file);
        }
      };

      _proto.addFile = function addFile(file) {
        var newFile = document.createElement('div');
        newFile.setAttribute('id', this.id + '-file-' + this._files.length.toString());
        newFile.className = 'file border border-primary';
        var title = document.createElement('div');
        title.className = 'title';
        title.innerText = file.name;
        var progress = document.createElement('div');
        progress.className = 'progress';
        var progressBar = document.createElement('div');
        progressBar.className = 'progress-bar progress-bar-striped progress-bar-animated';
        progressBar.setAttribute('role', 'progressbar');
        progressBar.setAttribute('aria-valuenow', '0');
        progressBar.innerText = '0%';
        progressBar.style.width = '0%';
        progressBar.setAttribute('aria-valuemax', '100');
        newFile.appendChild(title);
        newFile.appendChild(progress);
        progress.appendChild(progressBar);
        var temp = {
          id: this._files.length,
          file: file,
          status: newFile,
          progress: progressBar
        };

        this._files.push(temp);

        this._input.files.appendChild(newFile);

        var event = document.createEvent('event');
        event.initEvent('add', true, true);

        this._input.files.dispatchEvent(event);
      };

      _proto._startUpload = function _startUpload() {
        if (this._uploadRun === false && this._files.length > this._uploadFileIndex) {
          this._uploadRun = true;
          var index = this._uploadFileIndex;
          this._uploadFileIndex++;

          this._continueUpload(0, index);
        }
      };

      _proto._continueUpload = function _continueUpload(start, index) {
        var that = this,
            file = this._files[index],
            max = file.file.size,
            step = Math.pow(2, 19),
            end = start + step,
            isEnd = false;
        var current = 0;

        if (end > max) {
          end = max;
          isEnd = true;
          current = 100;
        } else {
          current = Math.floor(start * 100 / max);
        }

        file.progress.setAttribute('aria-valuenow', current.toString());
        file.progress.innerText = current.toString() + '%';
        file.progress.style.width = current.toString() + '%';

        var part = this._getPartOfFile(file.file, start, end);

        var form = new FormData();
        form.append('uid', this.id + '-' + index.toString());
        form.append('attached_data', this.attachedData);
        form.append('name', file.file.name);
        form.append('final', isEnd.toString());
        form.append('start', start);
        form.append('end', end);
        form.append('part_index', Math.floor(start / step));
        form.append('part_count', Math.floor(max / step) + 1);
        form.append('type', file.file.type);
        form.append('file', part);

        this._sendRequest({
          data: form,
          success: function success() {
            if (isEnd) {
              file.status.classList.add('border-success');
              file.status.classList.remove('border-primary');
              that._uploadRun = false;

              that._startUpload();
            } else {
              that._continueUpload(end, index);
            }
          },
          error: function error() {
            file.status.classList.add('border-danger');
            file.status.classList.remove('border-primary');
            that._uploadRun = false;

            that._startUpload();
          }
        });
      };

      _proto._getPartOfFile = function _getPartOfFile(file, start, end) {
        var slice = file.mozSlice ? file.mozSlice : file.webkitSlice ? file.webkitSlice : file.slice ? file.slice : function () {};
        return slice.bind(file)(start, end);
      };

      _proto._sendRequest = function _sendRequest(config) {
        config = _extends({
          data: {},
          type: 'POST',
          async: true,
          contentType: null,
          success: undefined,
          error: undefined,
          ajax: true
        }, config);
        var xhr;

        if (window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        } else {
          xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
        }

        xhr.open(config.type, this.destination, true);

        if (config.contentType) {
          xhr.setRequestHeader('Content-Type', config.contentType);
        }

        if (config.ajax) {
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }

        xhr.onload = function () {
          if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            Nette.parseResponse(response);

            if (config.success !== undefined) {
              config.success(xhr.responseText);
            }
          } else {
            if (config.error !== undefined) {
              config.error(xhr.responseText);
            }
          }
        };

        if (config.type === 'POST') {
          var data = '';
          if (config.contentType === 'application/json') data = JSON.stringify(config.data);else data = config.data;
          xhr.send(data);
        } else {
          xhr.send();
        }
      };

      _createClass(Uploader, [{
        key: "id",
        get: function get() {
          if (this._options.id === null) {
            var s4 = function s4() {
              return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            };

            this._options.id = s4() + s4() + '-' + s4() + s4() + '-' + s4() + s4();
          }

          return this._options.id;
        }
      }, {
        key: "destination",
        get: function get() {
          return this._options.srcElement.dataset.dest;
        }
      }, {
        key: "title",
        get: function get() {
          var data = this._options.srcElement.dataset;

          if (!data.hasOwnProperty('title')) {
            return 'Title';
          } else {
            return data.title;
          }
        }
      }, {
        key: "closeButton",
        get: function get() {
          var data = this._options.srcElement.dataset;

          if (!data.hasOwnProperty('close')) {
            return 'Close';
          } else {
            return data.close;
          }
        }
      }, {
        key: "attachedData",
        get: function get() {
          if ('attached' in this._options.srcElement.dataset) {
            return this._options.srcElement.dataset.attached;
          } else {
            return '{}';
          }
        }
      }, {
        key: "isMultiple",
        get: function get() {
          if ('multiple' in this._options.srcElement.dataset) {
            var val = this._options.srcElement.dataset.multiple;
            return val === 'true' || val === 'on' || val === 'enable';
          } else {
            return this._options.multiple;
          }
        }
      }, {
        key: "accept",
        get: function get() {
          if ('accept' in this._options.srcElement.dataset) {
            return this._options.srcElement.dataset.multiple;
          } else {
            return this._options.accept;
          }
        }
      }]);

      return Uploader;
    }();

    document.addEventListener('click', function (event) {
      var target = event.target;
      var parent = event.target.parentElement;

      if (target.hasAttribute('data-toggle') && target.dataset.toggle === 'uploader') {
        target.Uploader = new Uploader(target);
      } else if (parent !== null && parent.hasAttribute('data-toggle') && parent.dataset.toggle === 'uploader') {
        parent.Uploader = new Uploader(parent);
      }
    });

    if (typeof window.Uploader === 'undefined') {
      window.Uploader = Uploader;
    }

    return Uploader;
  }(window, document);

  exports.FileView = LightBox;
  exports.TreeView = TreeView;
  exports.Uploader = Uploader;

  return exports;

}({}));
