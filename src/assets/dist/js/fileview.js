/*
 * Alpha plugins - DiskBrowser.FileView
 * Builded: Sat Oct 20 2018 11:54:04 GMT+0000 (UTC)
 */
var FileView = (function () {
  'use strict';

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  var LightBox = function (window, document) {
    var LightBox =
    /*#__PURE__*/
    function () {
      function LightBox(handler, id) {
        this.options = {
          id: id,
          handler: handler,
          order: 0,
          idSuffix: '-lightbox',
          css: {
            footerHeight: 100,
            headerHeight: 32
          }
        };
        this.items = [];
        this.box = {};
        this.init();
      }

      var _proto = LightBox.prototype;

      _proto.init = function init() {
        this._createBox();

        this._changeImage(this.options.handler.dataset.src_large);

        this._initFooter();

        this._updateFooter();
      };

      _proto._updateImage = function _updateImage() {
        this.options.order = (this.items.length + this.options.order) % this.items.length;
        var img = this.items[this.options.order];

        this._changeImage(img.dataset.src_large);
      };

      _proto._changeImage = function _changeImage(url) {
        var content = this._getContent();

        this.box.img = document.createElement('img');
        this.box.img.setAttribute('src', url);
        this.box.img.style['max-height'] = this.maxHeightImg.toString() + 'px';
        this.box.img.style['max-width'] = this.maxWidthImg.toString() + 'px';

        while (content.firstChild) {
          content.removeChild(content.firstChild);
        }

        content.appendChild(this.box.img);
      };

      _proto.close = function close() {
        this.box.box.remove();
      };

      _proto._createBox = function _createBox() {
        var lightbox = document.createElement('div');
        var header = document.createElement('header');
        var body = document.createElement('div');
        var footer = document.createElement('footer');
        lightbox.setAttribute('id', this.getId);
        lightbox.setAttribute('class', 'lightbox');
        footer.style.height = this.options.css.footerHeight;
        header.style.height = this.options.css.headerHeight;
        body.setAttribute('class', 'body');
        body.style.height = '100%';
        var close = document.createElement('span');
        close.setAttribute('class', 'close cursor');
        var self = this;
        close.addEventListener('click', function () {
          self.close();
        });
        close.innerHTML = '&times;';
        header.appendChild(close);
        var content = document.createElement('div');
        content.setAttribute('class', 'content');
        var next = document.createElement('span');
        next.setAttribute('class', 'next fa fa-chevron-right');
        next.addEventListener('click', function () {
          self.options.order++;

          self._updateImage();

          self._updateFooter();
        });
        var prev = document.createElement('span');
        prev.setAttribute('class', 'prev fa fa-chevron-left');
        prev.addEventListener('click', function () {
          self.options.order--;

          self._updateImage();

          self._updateFooter();
        });
        body.appendChild(content);
        body.appendChild(prev);
        body.appendChild(next);
        window.addEventListener('resize', function () {
          self.box.img.style['max-height'] = self.maxHeightImg.toString() + 'px';
          self.box.img.style['max-width'] = self.maxWidthImg.toString() + 'px';
        }); // Add main childs

        lightbox.appendChild(header);
        lightbox.appendChild(body);
        lightbox.appendChild(footer);
        this.box = {
          body: body,
          box: lightbox,
          close: close,
          content: content,
          footer: footer,
          header: header,
          next: next,
          prev: prev
        };
        document.body.appendChild(lightbox);
      };

      _proto._initFooter = function _initFooter() {
        var footer = this._getFooter();

        var images = document.querySelector(this.options.id).querySelectorAll('.lightbox-item');
        var index = 0;
        var self = this;
        images.forEach(function (image) {
          self.items.push(image);
          var img = document.createElement('img');
          img.setAttribute('src', image.dataset.src_small);
          img.setAttribute('data-large', image.dataset.src_large);
          img.setAttribute('data-order', index);
          img.addEventListener('click', function () {
            self.options.order = img.dataset.order;

            self._updateFooter();

            self._changeImage(img.dataset.large);
          });
          footer.appendChild(img);

          if (self.options.handler === image) {
            self.options.order = index;
          }

          index++;
        });
        this.count = index;
      };

      _proto._updateFooter = function _updateFooter() {
        this.options.order = (this.items.length + this.options.order) % this.items.length;

        var footer = this._getFooter();

        var thumbs = footer.querySelectorAll('img');
        var index = 0;
        var self = this;
        thumbs.forEach(function (thumb) {
          if (index === self.options.order) {
            thumb.setAttribute('class', 'active');
          } else {
            thumb.removeAttribute('class');
          }

          index++;
        });
      };

      _proto._getContent = function _getContent() {
        return this.box.content;
      };

      _proto._getFooter = function _getFooter() {
        return this.box.footer;
      };

      _createClass(LightBox, [{
        key: "maxHeightImg",
        get: function get() {
          var styles = window.getComputedStyle(this.box.content, null);
          return this.box.content.offsetHeight - parseInt(styles.paddingTop) - parseInt(styles.paddingBottom);
        }
      }, {
        key: "maxWidthImg",
        get: function get() {
          return this.box.body.offsetWidth - this.box.prev.offsetWidth - this.box.next.offsetWidth;
        }
      }, {
        key: "getId",
        get: function get() {
          return this.options.id + this.options.idSuffix;
        }
      }]);

      return LightBox;
    }();

    document.addEventListener('click', function (event) {
      var target = event.target;
      var parent = event.target.parentElement;

      if (target.hasAttribute('data-toggle') && target.dataset.toggle === 'lightBox') {
        target.LightBox = new LightBox(target, target.datasets.filesId);
      } else if (parent !== null && parent.hasAttribute('data-toggle') && parent.dataset.toggle === 'lightBox') {
        parent.LightBox = new LightBox(parent, parent.dataset.filesId);
      }
    });

    if (typeof window.LightBox === 'undefined') {
      window.LightBox = LightBox;
    }

    return LightBox;
  }(window, document);

  return LightBox;

}());
