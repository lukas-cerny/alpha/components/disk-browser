/*
 * Alpha plugins - DiskBrowser.Uploader
 * Builded: Sun Oct 21 2018 16:39:07 GMT+0000 (UTC)
 */
var Uploader = (function () {
  'use strict';

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  /**
   * Base function for support Nette framework
   * @author Lukáš Černý
   */
  var Nette = function (document, window) {
    var Nette =
    /*#__PURE__*/
    function () {
      function Nette() {}

      Nette.parseResponse = function parseResponse(response) {
        if (response.snippets) {
          Nette.updateSnippets(response.snippets);
        }

        if (response.redirect) {
          Nette.redirect(response.redirect);
        }
      };

      Nette.redirect = function redirect(location) {
        document.location.replace(location);
      };

      Nette.updateSnippets = function updateSnippets(snippets) {
        for (var snippet in snippets) {
          var $el = document.getElementById(snippet);
          updateSnippet($el, snippets[snippet]);
        }

        function updateSnippet($el, html) {
          if ($el.querySelectorAll('title').length) {
            document.title = html;
          } else {
            if ($el.querySelectorAll('[data-ajax-append]').length) {
              $el.append(html);
            } else if ($el.querySelectorAll('[data-ajax-prepend]').length) {
              $el.prepend(html);
            } else {
              $el.innerHTML = html;
            }
          }
        }
      };

      return Nette;
    }();

    if (typeof window.Nette === 'undefined') {
      window.Nette = Nette;
    }

    return Nette;
  }(document, window);

  /**
   * Modal widnow for Bootstrap 4
   * @author Lukáš Černý
   */
  var Modal = function (document, window) {

    var EVENTS = {
      HIDE: 'hide',
      HIDE_PRE: 'pre_hide'
    };

    var Modal =
    /*#__PURE__*/
    function () {
      function Modal(id, content, config) {
        this._modal = undefined;
        this._backdrop = undefined;
        config = _extends({
          texts: {},
          config: {},
          elements: {}
        }, config);
        this._config = {
          id: id,
          class: 'modal fade',
          content: content
        };
        this._options = {
          texts: _extends({
            title: 'TITLE',
            closeButton: 'Close'
          }, config.texts),
          config: _extends({
            closeButton: true,
            footerButtons: []
          }, config.config),
          elements: _extends({
            title: 'h4'
          }, config.elements)
        };

        this._generateModal();

        this._generateBackdrop();
      }

      var _proto = Modal.prototype;

      _proto.show = function show() {
        if (typeof this._modal !== 'undefined') {
          document.body.classList.add('open-modal');
          var toShow = this.modal;

          if (!toShow.parentNode || toShow.parentNode.nodeType !== Node.ELEMENT_NODE) {
            document.body.appendChild(toShow);
            document.body.appendChild(this._backdrop);
          }

          toShow.style.display = 'block';
          toShow.classList.add('show');

          this._backdrop.classList.add('show');
        }
      };

      _proto.hide = function hide(event) {
        if (event) {
          event.preventDefault();
        }

        if (typeof this._modal !== 'undefined') {
          var toHide = this.modal;
          toHide.style.display = 'none';
          this._backdrop.style.display = 'none';
          toHide.setAttribute('aria-hidden', true);

          this._backdrop.setAttribute('aria-hidden', true);

          document.body.classList.remove('modal-open');
          var newEvent = document.createEvent('Event');
          newEvent.initEvent(Modal.eventHide, true, true);
          toHide.dispatchEvent(newEvent);
          toHide.remove();

          this._backdrop.remove();
        }
      };

      _proto._generateModal = function _generateModal() {
        var structure = {
          title: document.createElement(this._options.elements.title),
          header: document.createElement('div'),
          body: document.createElement('div'),
          footer: document.createElement('div'),
          content: document.createElement('div'),
          dialog: document.createElement('div')
        };

        for (var key in structure) {
          structure[key].className = 'modal-' + key;
        }

        var that = this;
        structure.title.innerText = this._options.texts.title;
        structure.header.appendChild(structure.title);
        var closeButton = document.createElement('button');
        closeButton.setAttribute('type', 'button');
        closeButton.setAttribute('class', 'close');
        closeButton.dataset.dismiss = 'modal';
        closeButton.innerHTML = '&times;';
        closeButton.addEventListener('click', function () {
          this.hide();
        }.bind(this));
        structure.header.appendChild(closeButton);

        if (typeof this._config.content === 'string') {
          structure.body.innerHTML = this._config.content;
        } else {
          structure.body.appendChild(this._config.content);
        }

        structure.content.appendChild(structure.header);
        structure.content.appendChild(structure.body);
        structure.content.appendChild(structure.footer);
        structure.dialog.appendChild(structure.content);
        structure.modal = document.createElement('div');
        structure.modal.className = this._config.class;
        structure.modal.setAttribute('id', this._config.id);
        structure.modal.appendChild(structure.dialog);

        this._options.config.footerButtons.forEach(function (button) {
          structure.footer.appendChild(button);
        });

        if (this._options.config.closeButton) {
          var closeButtonFooter = document.createElement('button');
          closeButtonFooter.setAttribute('type', 'button');
          closeButtonFooter.className = 'btn btn-default';
          closeButtonFooter.dataset.dismiss = 'modal';
          closeButtonFooter.innerHTML = this._options.texts.closeButton;
          closeButtonFooter.addEventListener('click', function () {
            that.hide();
          });
          structure.footer.appendChild(closeButtonFooter);
        }

        this._modal = structure;
      };

      _proto._generateBackdrop = function _generateBackdrop() {
        this._backdrop = document.createElement('div');
        this._backdrop.className = 'modal-backdrop fade';
      };

      _createClass(Modal, [{
        key: "modal",
        get: function get() {
          return this._modal.modal;
        }
      }], [{
        key: "eventHide",
        get: function get() {
          return EVENTS.HIDE;
        }
      }, {
        key: "eventHidePre",
        get: function get() {
          return EVENTS.HIDE_PRE;
        }
      }]);

      return Modal;
    }();

    if (typeof window.Modal === 'undefined') {
      window.Modal = Modal;
    }

    return Modal;
  }(document, window);

  var Uploader = function (window, document) {
    var Uploader =
    /*#__PURE__*/
    function () {
      function Uploader(element) {
        this._options = {
          id: null,
          srcElement: element,
          multiple: true,
          popup: true,
          accept: null
        };
        this._uploadRun = false;
        this._uploadFileIndex = 0;
        this._files = [];
        this._modal = undefined;
        this._input = undefined;

        this._init();
      }

      var _proto = Uploader.prototype;

      _proto._init = function _init() {
        this._generateForm();

        this._modal = new Modal(this.id + '-modal', this._input.container, {
          texts: {
            title: this.title,
            closeButton: this.closeButton
          }
        });

        this._modal.show();
      };

      _proto._generateForm = function _generateForm() {
        var structure = {
          container: document.createElement('div'),
          inputDiv: document.createElement('div'),
          input: document.createElement('input'),
          label: document.createElement('label'),
          files: document.createElement('div')
        };
        structure.inputDiv.className = 'custom-file row';
        structure.container.className = 'upload';
        structure.files.className = 'files row';
        structure.label.className = 'custom-file-label';
        structure.input.className = 'custom-file-input';
        structure.label.setAttribute('for', this.id + '-input');
        structure.input.setAttribute('id', this.id + '-input');
        structure.input.setAttribute('type', 'file');

        if (this.isMultiple) {
          structure.input.setAttribute('multiple', '');
        }

        if (this.accept !== null) {
          structure.input.setAttribute('accept', this.accept);
        }

        var that = this;
        structure.input.addEventListener('change', function (event) {
          event.preventDefault();
          that.inputChange();
        });
        structure.files.addEventListener('add', function (event) {
          event.preventDefault();

          that._startUpload();
        });
        structure.inputDiv.appendChild(structure.input);
        structure.inputDiv.appendChild(structure.label);
        structure.container.appendChild(structure.inputDiv);
        structure.container.appendChild(structure.files);
        this._input = structure;
      };

      _proto.inputChange = function inputChange() {
        var input = this._input.input;

        for (var i = 0; i < input.files.length; i++) {
          var file = input.files[i];
          this.addFile(file);
        }
      };

      _proto.addFile = function addFile(file) {
        var newFile = document.createElement('div');
        newFile.setAttribute('id', this.id + '-file-' + this._files.length.toString());
        newFile.className = 'file border border-primary';
        var title = document.createElement('div');
        title.className = 'title';
        title.innerText = file.name;
        var progress = document.createElement('div');
        progress.className = 'progress';
        var progressBar = document.createElement('div');
        progressBar.className = 'progress-bar progress-bar-striped progress-bar-animated';
        progressBar.setAttribute('role', 'progressbar');
        progressBar.setAttribute('aria-valuenow', '0');
        progressBar.innerText = '0%';
        progressBar.style.width = '0%';
        progressBar.setAttribute('aria-valuemax', '100');
        newFile.appendChild(title);
        newFile.appendChild(progress);
        progress.appendChild(progressBar);
        var temp = {
          id: this._files.length,
          file: file,
          status: newFile,
          progress: progressBar
        };

        this._files.push(temp);

        this._input.files.appendChild(newFile);

        var event = document.createEvent('event');
        event.initEvent('add', true, true);

        this._input.files.dispatchEvent(event);
      };

      _proto._startUpload = function _startUpload() {
        if (this._uploadRun === false && this._files.length > this._uploadFileIndex) {
          this._uploadRun = true;
          var index = this._uploadFileIndex;
          this._uploadFileIndex++;

          this._continueUpload(0, index);
        }
      };

      _proto._continueUpload = function _continueUpload(start, index) {
        var that = this,
            file = this._files[index],
            max = file.file.size,
            step = Math.pow(2, 19),
            end = start + step,
            isEnd = false;
        var current = 0;

        if (end > max) {
          end = max;
          isEnd = true;
          current = 100;
        } else {
          current = Math.floor(start * 100 / max);
        }

        file.progress.setAttribute('aria-valuenow', current.toString());
        file.progress.innerText = current.toString() + '%';
        file.progress.style.width = current.toString() + '%';

        var part = this._getPartOfFile(file.file, start, end);

        var form = new FormData();
        form.append('uid', this.id + '-' + index.toString());
        form.append('attached_data', this.attachedData);
        form.append('name', file.file.name);
        form.append('final', isEnd.toString());
        form.append('start', start);
        form.append('end', end);
        form.append('part_index', Math.floor(start / step));
        form.append('part_count', Math.floor(max / step) + 1);
        form.append('type', file.file.type);
        form.append('file', part);

        this._sendRequest({
          data: form,
          success: function success() {
            if (isEnd) {
              file.status.classList.add('border-success');
              file.status.classList.remove('border-primary');
              that._uploadRun = false;

              that._startUpload();
            } else {
              that._continueUpload(end, index);
            }
          },
          error: function error() {
            file.status.classList.add('border-danger');
            file.status.classList.remove('border-primary');
            that._uploadRun = false;

            that._startUpload();
          }
        });
      };

      _proto._getPartOfFile = function _getPartOfFile(file, start, end) {
        var slice = file.mozSlice ? file.mozSlice : file.webkitSlice ? file.webkitSlice : file.slice ? file.slice : function () {};
        return slice.bind(file)(start, end);
      };

      _proto._sendRequest = function _sendRequest(config) {
        config = _extends({
          data: {},
          type: 'POST',
          async: true,
          contentType: null,
          success: undefined,
          error: undefined,
          ajax: true
        }, config);
        var xhr;

        if (window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        } else {
          xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
        }

        xhr.open(config.type, this.destination, true);

        if (config.contentType) {
          xhr.setRequestHeader('Content-Type', config.contentType);
        }

        if (config.ajax) {
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }

        xhr.onload = function () {
          if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            Nette.parseResponse(response);

            if (config.success !== undefined) {
              config.success(xhr.responseText);
            }
          } else {
            if (config.error !== undefined) {
              config.error(xhr.responseText);
            }
          }
        };

        if (config.type === 'POST') {
          var data = '';
          if (config.contentType === 'application/json') data = JSON.stringify(config.data);else data = config.data;
          xhr.send(data);
        } else {
          xhr.send();
        }
      };

      _createClass(Uploader, [{
        key: "id",
        get: function get() {
          if (this._options.id === null) {
            var s4 = function s4() {
              return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            };

            this._options.id = s4() + s4() + '-' + s4() + s4() + '-' + s4() + s4();
          }

          return this._options.id;
        }
      }, {
        key: "destination",
        get: function get() {
          return this._options.srcElement.dataset.dest;
        }
      }, {
        key: "title",
        get: function get() {
          var data = this._options.srcElement.dataset;

          if (!data.hasOwnProperty('title')) {
            return 'Title';
          } else {
            return data.title;
          }
        }
      }, {
        key: "closeButton",
        get: function get() {
          var data = this._options.srcElement.dataset;

          if (!data.hasOwnProperty('close')) {
            return 'Close';
          } else {
            return data.close;
          }
        }
      }, {
        key: "attachedData",
        get: function get() {
          if ('attached' in this._options.srcElement.dataset) {
            return this._options.srcElement.dataset.attached;
          } else {
            return '{}';
          }
        }
      }, {
        key: "isMultiple",
        get: function get() {
          if ('multiple' in this._options.srcElement.dataset) {
            var val = this._options.srcElement.dataset.multiple;
            return val === 'true' || val === 'on' || val === 'enable';
          } else {
            return this._options.multiple;
          }
        }
      }, {
        key: "accept",
        get: function get() {
          if ('accept' in this._options.srcElement.dataset) {
            return this._options.srcElement.dataset.multiple;
          } else {
            return this._options.accept;
          }
        }
      }]);

      return Uploader;
    }();

    document.addEventListener('click', function (event) {
      var target = event.target;
      var parent = event.target.parentElement;

      if (target.hasAttribute('data-toggle') && target.dataset.toggle === 'uploader') {
        target.Uploader = new Uploader(target);
      } else if (parent !== null && parent.hasAttribute('data-toggle') && parent.dataset.toggle === 'uploader') {
        parent.Uploader = new Uploader(parent);
      }
    });

    if (typeof window.Uploader === 'undefined') {
      window.Uploader = Uploader;
    }

    return Uploader;
  }(window, document);

  return Uploader;

}());
