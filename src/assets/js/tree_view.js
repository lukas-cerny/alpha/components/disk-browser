import Base from '../../../../assets/src/js/base';
import Modal from '../../../../assets/src/js/modal';
import ContextMenu from '../../../../assets/src/js/context_menu';

const TreeView = function (window, document) {

    class TreeView extends Base {
        constructor(element, config) {
            super();
            this.rootInitialized = false;
            this.wrapper = undefined;
            this.structure = {};
            this.nodes = {};
            this.invokedOn = element;
            this.contextMenu = undefined;
            this.options = {
                ajax: Object.assign({}, config.ajax),
                config: Object.assign({
                    rootId: -1,
                    addRoot: false,
                    rootName: '/',
                    debug: true,
                    highlightSelected: true,
                    showBorder: true,
                    showHover: true,
                    sort: true,
                }, config.config || {}),
                contextMenu: Object.assign({
                    enabled: true,
                    items: [],
                    removeTitle: 'Delete node',
                    removeMessage: 'Do you really want to remove:',
                    buttonYes: 'Yes',
                    buttonNo: 'No'
                }, config.contextMenu || {}),
                icons: Object.assign({
                    expand: 'fa fa-minus',
                    collapse: 'fa fa-plus',
                    empty: 'fa',
                    node: '',
                    selected: '',
                    sendInputForm: 'fa fa-check',
                    cancelInputForm: 'fa fa-times'
                }, config.icons || {}),
                error: Object.assign({
                    title: 'Error!!!',
                    button: 'OK'
                }, config.error || {}),
                events: Object.assign({
                    onNodeCollapsed: undefined,
                    onNodeExpanded: undefined,
                    onNodeSelected: undefined,
                    onNodeUnselected: undefined
                }, config.events || {}),
                initStates: Object.assign({
                    expanded: false
                }, config.initStates || {})
            };
            this.init();
        }

        init(data) {
            if (data === undefined) {
                if (!(this.options.data) && typeof this.options.ajax.root === 'string') {
                    this.sendRequest({
                        destination: this.options.ajax.root,
                        success: function (response) {
                            this.init(response);
                        }.bind(this)
                    });
                } else {
                    this.options.data = this.options.data || [];
                    this.init(this.options.data);
                }
            } else {
                if (typeof data === 'string') {
                    data = JSON.parse(data);
                }
                if (this.options.data) {
                    delete this.options.data;
                }

                if (this.options.config.addRoot || !data.hasOwnProperty('id')) {
                    this.rootInitialized = true;
                    this._initNode({
                        id: this.options.config.rootId,
                        text: this.options.config.rootName,
                        selectable: false,
                        states: {
                            expanded: true,
                        },
                        nodes: []
                    }, undefined);
                }

                if (data.hasOwnProperty('id')) {
                    this._initNode(data, undefined, 0);
                } else {
                    this._initNodes(data, this.options.config.rootId, 0);
                }

                this._eventsSubscribe();
                this.render();
            }
        }

        _getTemplate(type) {
            var template = {
                list: {
                    elem: 'ul',
                    class: 'list-group'
                },
                item: {
                    elem: 'li',
                    class: 'list-group-item'
                },
                indent: {
                    elem: 'span',
                    class: 'indent'
                },
                icon: {
                    elem: 'span',
                    class: 'icon',
                },
                innerText: {
                    elem: 'span',
                    class: ''
                }
            };
            let elem = document.createElement(template[type].elem);
            elem.setAttribute('class', template[type].class);
            return elem;
        }

        _initNode(node, parentId, indent) {
            if (!node)
                return undefined;

            var item = this.getNode(node.id) || {
                id: node.id,
                parentId: parentId,
                text: node.text,
                selectable: node.selectable !== undefined ? node.selectable : true,
                indent: indent,
                colors: node.colors || {},
                nodes: [],
                states: node.states || {
                    selected: false
                }
            };

            if (!item.states.hasOwnProperty('expanded')) {
                if (node.nodes && node.nodes.length > 0) {
                    item.states.expanded = this.options.initStates.expanded;
                } else {
                    item.states.expanded = false;
                }
            }

            this.nodes[item.id] = item;

            if (parentId) {
                let parent = this.getNode(parentId);
                parent.nodes.push(item);
                this._sortNodes(parent);

            } else if (this.options.config.addRoot && item.id !== this.options.config.rootId) {
                item.parentId = this.options.config.rootId;
                this.getNode(this.options.config.rootId).nodes.push(item);
            } else {
                this.structure = item;
            }

            this._buildNode(item);
            this._sortNodes(item);
            this._initNodes(node.nodes, item.id, indent + 1);

            return item;
        }

        _initNodes(nodes, parentId, indent) {
            if (!Array.isArray(nodes) || nodes.length === 0) {
                return;
            }

            nodes.forEach(function (node) {
                this._initNode(node, parentId, indent);
            }.bind(this));
        }

        _updateNode(node) {
            if (!node)
                return;

            var item = node.element;
            if (node.states.selected) {
                item.classList.add('selected');
                item.classList.remove('hover');
            } else {
                item.classList.remove('selected');
                if (this.options.config.showHover) {
                    item.classList.add('hover');
                }
            }

            node.elementText.innerText = node.text;

            let parent = this.getNode(node.parentId);
            if (parent && !parent.states.expanded) {
                item.setAttribute('hidden', true);
            } else if (parent && parent.states.expanded) {
                item.removeAttribute('hidden');
            } else if (!parent && !this.options.config.addRoot && this.rootInitialized) {
                item.setAttribute('hidden', true);
            }

            let icon = node.elementIcon;
            let add, remove = [];
            if (node.nodes) {
                if (node.states.expanded) {
                    add = this.options.icons.expand.split(' ');
                    remove = this.options.icons.collapse.split(' ');
                } else {
                    remove = this.options.icons.expand.split(' ');
                    add = this.options.icons.collapse.split(' ');
                }
            }
            remove.forEach(function (className) {
                icon.classList.remove(className);
            });
            add.forEach(function (className) {
                icon.classList.add(className);
            });
        }

        _buildNode(node) {
            let item = this._getTemplate('item');
            let icon = this._getTemplate('icon');
            let text = this._getTemplate('innerText');
            node.element = item;
            node.elementIcon = icon;
            node.elementText = text;

            item.setAttribute('data-id', node.id);
            item.appendChild(icon);
            item.appendChild(text);

            this._updateNode(node);
        }

        _sortNodes(node) {
            if (this.options.config.sort) {
                node.nodes = node.nodes.sort(function (a, b) {
                    if (a.text === b.text) {
                        return a.id < b.id;
                    } else {
                        return a.text > b.text;
                    }
                });
            }
        }

        render() {
            let el = this.invokedOn;

            while (el.firstChild)
                el.removeChild(el.firstChild);

            el.classList.add('treeView');
            this.wrapper = this._getTemplate('list');

            el.appendChild(this.wrapper);
            this._buildTree(this.structure, 0);
        }

        _buildTree(node) {
            if (!node)
                return;
            let icon = node.elementIcon,
                parent = this.getNode(node.parentId);

            var indent = 0;
            if (node.parentId) {
                if (!(node.parentId === this.options.config.rootId && !this.options.config.addRoot && this.rootInitialized)) {
                    indent = parent.indent + 1;
                }
            }
            node.indent = indent;
            for (let i = 0; i < indent; i++) {
                icon.before(this._getTemplate('indent'));
            }

            this._sortNodes(node);

            if (node.parentId === undefined || node.parentId === this.structure.id) {
                this.wrapper.appendChild(node.element);
            } else {
                for (let i = 0; i < parent.nodes.length; i++) {
                    let tempNode = parent.nodes[i];
                    if (tempNode.id === node.id) {
                        if (i > 0) {
                            parent.nodes[i - 1].element.after(node.element);
                        } else {
                            parent.element.after(node.element);
                        }
                        break;
                    }
                }
            }
            node.nodes.forEach(this._buildTree.bind(this));
        }

        _eventsSubscribe() {
            this.addEventListener(this.invokedOn, 'click', this._clickHandler, this);
            this.addEventListener(this.invokedOn, 'nodeExpanded', this.options.events.onNodeExpanded, this);
            this.addEventListener(this.invokedOn, 'nodeCollapsed', this.options.events.onNodeCollapsed, this);
            this.addEventListener(this.invokedOn, 'nodeSelected', this.options.events.onNodeSelected, this);
            this.addEventListener(this.invokedOn, 'nodeUnselected', this.options.events.onNodeUnselected, this);

            if (this.options.contextMenu.enabled) {
                this.addEventListener(this.invokedOn, 'contextmenu', this._contextMenu, this);
            } else {
                this.addEventListener(this.invokedOn, 'contextmenu', function () {
                });
            }
        }

        _eventsUnsubscribe() {
            this.removeEventListener(this.invokedOn, 'nodeExpanded', this.options.events.onNodeExpanded, this);
            this.removeEventListener(this.invokedOn, 'nodeCollapsed', this.options.events.onNodeCollapsed, this);
            this.removeEventListener(this.invokedOn, 'nodeSelected', this.options.events.onNodeSelected, this);
            this.removeEventListener(this.invokedOn, 'nodeUnselected', this.options.events.onNodeUnselected, this);
            if (this.options.contextMenu.enabled) {
                this.removeEventListener(this.invokedOn, 'contextmenu', this._contextMenu, this);
                this.removeEventListener(document, 'click', this._contextMenuHide, this);
            } else {
                this.removeEventListener(this.invokedOn, 'contextmenu', function () {
                    return false;
                }, this);
            }
        }

        _clickHandler(event) {
            let target = event.target;
            let tag = target.tagName;
            if (tag === 'input' || tag === 'button' || target.parentElement.tagName === 'button')
                return;

            let node = this.findNode(target),
                classList = target.classList;

            if (!node)
                return;

            if (classList.contains('icon')) {
                this._toggleExpandedState(node);
            } else {
                if (node.selectable) {
                    this._toggleSelectedState(node);
                } else {
                    this._toggleExpandedState(node);
                }
            }
        }

        _toggleExpandedState(node) {
            if (!node)
                return;
            this._setExpandedState(node, !node.states.expanded);
        }

        _toggleSelectedState(node) {
            if (!node)
                return;
            this._setSelectedState(node, !node.states.selected);
        }

        _setExpandedState(node, state) {
            if (state && state === node.states.expanded)
                return;

            if (state && node.nodes) {
                node.states.expanded = true;
                node.nodes.forEach(function (subNode) {
                    this._updateNode(subNode);
                }.bind(this));
                let event = new CustomEvent('nodeExpanded', {
                    detail: node
                });
                this.invokedOn.dispatchEvent(event);
            } else if (!state) {
                node.states.expanded = false;
                let event = new CustomEvent('nodeCollapsed', {
                    detail: node
                });
                this.invokedOn.dispatchEvent(event);
                if (node.nodes) {
                    node.nodes.forEach(function (subNode) {
                        this._setExpandedState(subNode, false);
                    }.bind(this));
                }
            }
            this._updateNode(node);
        }

        _setSelectedState(node, state) {
            if (state === node.states.selected)
                return;
            var event;
            if (state) {
                this.getSelected().forEach(function (subNode) {
                    this._setSelectedState(subNode, false);
                }.bind(this));
                event = new CustomEvent('nodeSelected', {
                    detail: node
                });
            } else {
                event = new CustomEvent('nodeUnselected', {
                    detail: node
                });
            }
            this.invokedOn.dispatchEvent(event);
            node.states.selected = state;
            this._updateNode(node);
        }

        _findNodes(pattern, modifier, attribute) {
            modifier = modifier || 'g';
            attribute = attribute || 'text';

            let values = Object.keys(this.nodes).map(function (key) {
                return this.nodes[key];
            }.bind(this));
            return values.filter(function (node) {
                var val = this._getNodeValue(node, attribute);
                if (typeof val === 'string') {
                    return val.match(new RegExp(pattern, modifier));
                }
            }.bind(this));
        }

        _getNodeValue(obj, attr) {
            var index = attr.indexOf('.');
            if (index > 0) {
                var _obj = obj[attr.substring(0, index)];
                var _attr = attr.substring(index + 1, attr.length);
                return this._getNodeValue(_obj, _attr);
            } else {
                if (obj.hasOwnProperty(attr)) {
                    return obj[attr].toString();
                } else {
                    return undefined;
                }
            }
        }

        getNode(id) {
            if (this.nodes.hasOwnProperty(id)) {
                return this.nodes[id];
            } else {
                return undefined;
            }
        }

        findNode(target) {
            let closest = target.closest('li.list-group-item');
            if (!closest) {
                return undefined;
            }
            let id = closest.dataset.id,
                node = this.getNode(id);
            if (!node) {
                this._logError('Error: node does not exists');
            }
            return node;
        }

        _destroy() {
            if (this.initialized) {
                this.wrapper.remove();
                this.wrapper = null;
                this._eventsUnsubscribe();
                if (this.contextMenu) {
                    this.contextMenu.destroy();
                    this.contextMenu = undefined;
                }
                this.initialized = false;
            }
        }

        _contextMenu(event) {
            var target = event.target;
            var node = this.findNode(target);

            if (!node)
                return;

            this.options.contextMenu.items.forEach(function (item) {
                var callback = item.callback;
                if (item.id === 'create') {
                    item.callback = function (invokedOn) {
                        let data = this._contextMenuInputNode(),
                            modal = new Modal(this.elementId + '-add', data.input, {
                                texts: {
                                    title: item.text
                                },
                                config: {
                                    footerButtons: data.buttons
                                }
                            });
                        data.buttons.forEach(function (button) {
                            button.addEventListener('click', function (event) {
                                let target = event.target;
                                if (target.tagName === 'span') {
                                    target = target.parentNode;
                                }
                                if (data.input.value !== '') {
                                    modal.hide();
                                    this.node({
                                        type: 'create',
                                        parentId: invokedOn.id,
                                        text: data.input.value,
                                        callback: callback
                                    });
                                }
                            }.bind(this));
                        }.bind(this));
                        modal.show();
                    }.bind(this);
                } else if (item.id === 'rename') {
                    item.callback = function (invokedOn) {
                        let data = this._contextMenuInputNode(),
                            input = data.input.value = invokedOn.text,
                            modal = new Modal(this.elementId + '-rename', data.input, {
                                texts: {
                                    title: item.text
                                },
                                config: {
                                    footerButtons: data.buttons
                                }
                            });
                        data.buttons.forEach(function (button) {
                            button.addEventListener('click', function (event) {
                                let target = event.target;
                                if (target.tagName === 'span') {
                                    target = target.parentNode;
                                }
                                if (input.value !== invokedOn.text) {
                                    modal.hide();
                                    this.node({
                                        type: 'rename',
                                        id: invokedOn.id,
                                        text: data.input.value,
                                        callback: callback
                                    });
                                }
                            }.bind(this));
                        }.bind(this));
                        modal.show();
                    }.bind(this);
                } else if (item.id === 'delete') {
                    item.callback = function (invokedOn) {
                        if (!invokedOn)
                            return;
                        let data = this._contextMenuRemoveModal(invokedOn);
                        data.button.addEventListener('click', function () {
                            data.modal.hide();
                            this.node({
                                type: 'delete',
                                id: invokedOn.id,
                                callback: callback
                            });
                        }.bind(this));
                        data.modal.show();
                    }.bind(this);
                }
            }.bind(this));

            if (this.contextMenu) {
                this.contextMenu.destroy();
            }

            this.contextMenu = new ContextMenu(node, {
                items: this.options.contextMenu.items,
                position: {
                    left: event.pageX,
                    top: event.pageY
                }
            });
            this.contextMenu.show();
        }

        node(data) {
            var config = {
                data: new FormData(),
                error: function (data) {
                    var response = JSON.parse(data),
                        modal = this._displayErrorModal(response.message);
                    modal.show();
                }.bind(this)
            };
            if (data.type === 'create') {
                config.destination = this.options.ajax.create;
                config.data.append('parent_id', data.parentId);
                config.data.append('text', data.text);
                config.success = function (response) {
                    response = JSON.parse(response);
                    let parent = this.getNode(data.parentId),
                        node = {
                            id: response.id,
                            text: data.text,
                            parentId: data.parentId
                        };
                    node = this._initNode(node, data.parentId);
                    this._buildTree(node);
                    this._setExpandedState(parent, true);
                    this._setSelectedState(node, true);
                }.bind(this);
            } else if (data.type === 'rename') {
                config.destination = this.options.ajax.update;
                config.data.append('id', data.id.toString());
                config.data.append('text', data.text);
                config.success = function () {
                    var node = this.getNode(data.id);
                    node.text = data.text;
                    this._updateNode(node);
                }.bind(this);
            } else if (data.type === 'delete') {
                config.destination = this.options.ajax.delete;
                config.data.append('id', data.id);
                config.success = function () {
                    let node = this.getNode(data.id);
                    this._nodeDelete(node);
                }.bind(this);
            }
            this.sendRequest(config);
            if (data.callback) {
                data.callback();
            }
        }

        _nodeDelete(node) {
            if (!node) {
                return;
            }
            if (node.nodes && node.nodes.length > 0) {
                node.nodes.forEach(this._nodeDelete.bind(this));
            }
            node.element.remove();
            delete this.nodes[node.id];
            if (node.parentId !== undefined) {
                let parent = this.getNode(node.parentId);
                parent.nodes = parent.nodes.filter(function (value) {
                    return value.id !== node.id;
                });
            }
        }

        _contextMenuInputNode() {
            var btn = function (name, className, spanClass) {
                var btn = document.createElement('button'),
                    span = document.createElement('span');
                btn.setAttribute('type', 'button');
                btn.setAttribute('name', name);
                btn.setAttribute('class', 'btn ' + className);
                span.setAttribute('class', spanClass);
                btn.appendChild(span);
                return btn;
            };
            var input = document.createElement('input');
            input.setAttribute('class', 'form-control');
            input.setAttribute('type', 'text');
            input.setAttribute('name', 'name');
            return {input: input, buttons: [btn('send', 'btn-success', this.options.icons.sendInputForm)]};
        }

        _contextMenuRemoveModal(node) {
            var btn = document.createElement('button');
            btn.setAttribute('type', 'submit');
            btn.setAttribute('name', 'yes');
            btn.setAttribute('class', 'btn btn-success');
            btn.innerText = this.options.contextMenu.buttonYes;
            return {
                modal: new Modal(this.elementId + 'edit', this.options.contextMenu.removeMessage + ' ' + node.text, {
                    texts: {
                        title: this.options.contextMenu.removeTitle,
                        closeButton: this.options.contextMenu.buttonNo
                    },
                    config: {
                        footerButtons: [btn]
                    }
                }), button: btn
            };
        }

        _displayErrorModal(message) {
            var content = document.createElement('div');
            content.innerHTML = message;
            return new Modal(this.elementId, content, {
                texts: {
                    title: this.options.error.title,
                    closeButton: this.options.error.button
                }
            });
        }

        _getParent(identifier) {
            var node = this._identifyNode(identifier);
            return this.getNode(node.parentId);
        }

        _expandLevels(nodes) {
            nodes.forEach(function (node) {
                this._setExpandedState(node, true);
                if (node.nodes) {
                    this._expandLevels(node.nodes);
                }
            }.bind(this));
        }

        _forEachIdentifier(identifiers, callback) {
            if (!(identifiers instanceof Array)) {
                identifiers = [identifiers];
            }
            identifiers.forEach(function (identifier) {
                callback(this._identifyNode(identifier));
            }.bind(this));
        }

        _identifyNode(identifier) {
            if (typeof identifier === 'number') {
                return this.getNode(identifier);
            } else {
                return identifier;
            }
        }

        remove() {
            this._destroy();
        }

        toggleNodeExpanded(identifiers) {
            this._forEachIdentifier(identifiers, this._toggleExpandedState.bind(this));
        }

        toggleNodeSelected(identifiers) {
            this._forEachIdentifier(identifiers, this._toggleSelectedState.bind(this));
        }

        selectNode(identifiers) {
            this._forEachIdentifier(identifiers, function (node) {
                this._setSelectedState(node, true);
            }.bind(this));
        }

        unselectNode(identifiers) {
            this._forEachIdentifier(identifiers, function (node) {
                this._setSelectedState(node, false);
            }.bind(this));
        }

        collapseAll() {
            this._forEachIdentifier(this.getExpanded(), function (node) {
                this._setExpandedState(node, false);
            }.bind(this));
        }

        expandAll() {
            this._forEachIdentifier(this.getCollapsed(), function (node) {
                this._setSelectedState(node, true);
            }.bind(this));
        }

        getSelected() {
            return this._findNodes('true', 'g', 'states.selected');
        }

        getUnselected() {
            return this._findNodes('false', 'g', 'states.selected');
        }

        getExpanded() {
            return this._findNodes('true', 'g', 'states.expanded');
        }

        getCollapsed() {
            return this._findNodes('false', 'g', 'states.expanded');
        }
    }

    if (typeof(window.TreeView) === 'undefined') {
        window.TreeView = TreeView;
    }

    return TreeView;
}(window, document);

export default TreeView;
