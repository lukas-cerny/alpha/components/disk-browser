import Nette from '../../../../assets/src/js/nette';
import Modal from '../../../../assets/src/js/modal';

const Uploader = function (window, document) {

    class Uploader {
        constructor(element) {
            this._options = {
                id: null,
                srcElement: element,
                multiple: true,
                popup: true,
                accept: null
            };
            this._uploadRun = false;
            this._uploadFileIndex = 0;
            this._files = [];
            this._modal = undefined;
            this._input = undefined;
            this._init();
        }

        get id() {
            if (this._options.id === null) {
                var s4 = function () {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                };
                this._options.id = s4() + s4() + '-' + s4() + s4() + '-' + s4() + s4();
            }
            return this._options.id;
        }

        get destination() {
            return this._options.srcElement.dataset.dest;
        }

        get title() {
            var data = this._options.srcElement.dataset;
            if (!data.hasOwnProperty('title')) {
                return 'Title';
            } else {
                return data.title;
            }
        }

        get closeButton() {
            var data = this._options.srcElement.dataset;
            if (!data.hasOwnProperty('close')) {
                return 'Close';
            } else {
                return data.close;
            }
        }

        get attachedData() {
            if ('attached' in this._options.srcElement.dataset) {
                return this._options.srcElement.dataset.attached;
            } else {
                return '{}';
            }
        }

        get isMultiple() {
            if ('multiple' in this._options.srcElement.dataset) {
                var val = this._options.srcElement.dataset.multiple;
                return val === 'true' || val === 'on' || val === 'enable';
            } else {
                return this._options.multiple;
            }
        }

        get accept() {
            if ('accept' in this._options.srcElement.dataset) {
                return this._options.srcElement.dataset.multiple;
            } else {
                return this._options.accept;
            }
        }

        _init() {
            this._generateForm();
            this._modal = new Modal(this.id + '-modal', this._input.container, {
                texts: {
                    'title': this.title,
                    'closeButton': this.closeButton
                }
            });
            this._modal.show();
        }

        _generateForm() {
            var structure = {
                container: document.createElement('div'),
                inputDiv: document.createElement('div'),
                input: document.createElement('input'),
                label: document.createElement('label'),
                files: document.createElement('div')
            };
            structure.inputDiv.className = 'custom-file row';
            structure.container.className = 'upload';
            structure.files.className = 'files row';
            structure.label.className = 'custom-file-label';
            structure.input.className = 'custom-file-input';
            structure.label.setAttribute('for', this.id + '-input');
            structure.input.setAttribute('id', this.id + '-input');
            structure.input.setAttribute('type', 'file');
            if (this.isMultiple) {
                structure.input.setAttribute('multiple', '');
            }
            if (this.accept !== null) {
                structure.input.setAttribute('accept', this.accept);
            }
            var that = this;
            structure.input.addEventListener('change', function (event) {
                event.preventDefault();
                that.inputChange();
            });
            structure.files.addEventListener('add', function (event) {
                event.preventDefault();
                that._startUpload();
            });
            structure.inputDiv.appendChild(structure.input);
            structure.inputDiv.appendChild(structure.label);
            structure.container.appendChild(structure.inputDiv);
            structure.container.appendChild(structure.files);
            this._input = structure;
        }

        inputChange() {
            var input = this._input.input;
            for (var i = 0; i < input.files.length; i++) {
                var file = input.files[i];
                this.addFile(file);
            }
        }

        addFile(file) {
            var newFile = document.createElement('div');
            newFile.setAttribute('id', this.id + '-file-' + this._files.length.toString());
            newFile.className = 'file border border-primary';
            var title = document.createElement('div');
            title.className = 'title';
            title.innerText = file.name;
            var progress = document.createElement('div');
            progress.className = 'progress';
            var progressBar = document.createElement('div');
            progressBar.className = 'progress-bar progress-bar-striped progress-bar-animated';
            progressBar.setAttribute('role', 'progressbar');
            progressBar.setAttribute('aria-valuenow', '0');
            progressBar.innerText = '0%';
            progressBar.style.width = '0%';
            progressBar.setAttribute('aria-valuemax', '100');
            newFile.appendChild(title);
            newFile.appendChild(progress);
            progress.appendChild(progressBar);

            var temp = {
                id: this._files.length,
                file: file,
                status: newFile,
                progress: progressBar
            };
            this._files.push(temp);
            this._input.files.appendChild(newFile);
            var event = document.createEvent('event');
            event.initEvent('add', true, true);
            this._input.files.dispatchEvent(event);
        }

        _startUpload() {
            if (this._uploadRun === false
                && this._files.length > this._uploadFileIndex) {
                this._uploadRun = true;
                var index = this._uploadFileIndex;
                this._uploadFileIndex++;
                this._continueUpload(0, index);
            }
        }

        _continueUpload(start, index) {
            var that = this,
                file = this._files[index],
                max = file.file.size,
                step = Math.pow(2, 19),
                end = start + step,
                isEnd = false;
            var current = 0;

            if (end > max) {
                end = max;
                isEnd = true;
                current = 100;
            } else {
                current = Math.floor(start * 100 / max);
            }

            file.progress.setAttribute('aria-valuenow', current.toString());
            file.progress.innerText = current.toString() + '%';
            file.progress.style.width = current.toString() + '%';
            var part = this._getPartOfFile(file.file, start, end);
            var form = new FormData();
            form.append('uid', this.id + '-' + index.toString());
            form.append('attached_data', this.attachedData);
            form.append('name', file.file.name);
            form.append('final', isEnd.toString());
            form.append('start', start);
            form.append('end', end);
            form.append('part_index', Math.floor(start / step));
            form.append('part_count', Math.floor(max / step) + 1);
            form.append('type', file.file.type);
            form.append('file', part);
            this._sendRequest({
                data: form,
                success: function () {
                    if (isEnd) {
                        file.status.classList.add('border-success');
                        file.status.classList.remove('border-primary');
                        that._uploadRun = false;
                        that._startUpload();
                    } else {
                        that._continueUpload(end, index);
                    }
                },
                error: function() {
                    file.status.classList.add('border-danger');
                    file.status.classList.remove('border-primary');
                    that._uploadRun = false;
                    that._startUpload();
                }
            });
        }

        _getPartOfFile(file, start, end) {
            var slice = file.mozSlice ? file.mozSlice :
                file.webkitSlice ? file.webkitSlice :
                    file.slice ? file.slice : function () {
                    };
            return slice.bind(file)(start, end);
        }

        _sendRequest(config) {
            config = Object.assign({
                data: {},
                type: 'POST',
                async: true,
                contentType: null,
                success: undefined,
                error: undefined,
                ajax: true,
            }, config);

            var xhr;
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else {
                xhr = new window.ActiveXObject('Microsoft.XMLHTTP');
            }
            xhr.open(config.type, this.destination, true);
            if (config.contentType) {
                xhr.setRequestHeader('Content-Type', config.contentType);
            }
            if (config.ajax) {
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            }
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var response = JSON.parse(xhr.responseText);
                    Nette.parseResponse(response);
                    if (config.success !== undefined) {
                        config.success(xhr.responseText);
                    }
                } else {
                    if (config.error !== undefined) {
                        config.error(xhr.responseText);
                    }
                }
            };

            if (config.type === 'POST') {
                var data = '';
                if (config.contentType === 'application/json')
                    data = JSON.stringify(config.data);
                else
                    data = config.data;
                xhr.send(data);
            } else {
                xhr.send();
            }
        }
    }

    document.addEventListener('click', function (event) {
        var target = event.target;
        var parent = event.target.parentElement;

        if (target.hasAttribute('data-toggle') && target.dataset.toggle === 'uploader') {
            target.Uploader = new Uploader(target);
        } else if (parent !== null && parent.hasAttribute('data-toggle') && parent.dataset.toggle === 'uploader') {
            parent.Uploader = new Uploader(parent);
        }
    });

    if (typeof(window.Uploader) === 'undefined') {
        window.Uploader = Uploader;
    }

    return Uploader;

}(window, document);

export default Uploader;